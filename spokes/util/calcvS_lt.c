/***********************************************************************
* Program Name: calcvS.c
* Author:       Will Grissom
***********************************************************************/
#ifndef NOTHREADS
#include <pthread.h>
#endif
#include <math.h>
#include "mex.h"

/* Input Arguments */

#define	A	prhs[0] /* excitation vectors */ 
#define D       prhs[1] /* desired patterns */ 
#define XX      prhs[2] /* encoding vectors */
#define GM      prhs[3] /* gradient matrix? */
#define NB      prhs[4] /* vector of number of betas in each term */
#define BTH     prhs[5] /* beta threshold */
#define NT      prhs[6] /* number of threads */

/* Output Arguments */

#define	VXOUT	plhs[0]
#define SXOUT   plhs[1]
#define	VYOUT	plhs[2]
#define SYOUT   plhs[3]
#define	VZOUT	plhs[4]
#define SZOUT   plhs[5]

#define PI 3.14159265
#define GAMBAR 4257.

typedef struct {
  int m;
  int npe; /* number of phase encodes */
  int nt; /* number of terms (=col dim of A) */
  double *nbeta;
  double betath;
  double *Gm;
  double *Ar,*Ai;
  double *dr,*di;
  double *x,*y,*z;
  double *vx,*vy,*vz; /* each thread has its own copy of v, S; we sum these over space after all threads are done */
  double *Sx,*Sy,*Sz;
  int si,ei; /* start/end spatial indices */
} params;

void *docalcvS_lt(void *arg)
{
    
  params *p = (params *)arg;

  /* separable term */
  int i,j,k,l,n;
  int m = p->m;
  int npe = p->npe;
  double ajdr, ajdi, sjl, tmp, tmp2, tmp3;
  double *dr = p->dr;
  double *di = p->di;
  double *Ar = p->Ar;
  double *Ai = p->Ai;
  double *x = p->x;
  double *y = p->y;
  double *z = p->z;
  double *vx = p->vx;
  double *vy = p->vy;
  double *vz = p->vz;
  double *Sx = p->Sx;
  double *Sy = p->Sy;
  double *Sz = p->Sz;
  double *Gm = p->Gm;
  int si = p->si;
  int ei = p->ei;
  
  for(i = 0;i < p->nt;i++){
    if(p->nbeta[i] <= p->betath){
      /* loop over space */
      for(j = si;j < ei;j++){

	ajdr = dr[j]*Ar[i*m+j] + di[j]*Ai[i*m+j];
	ajdi = dr[j]*Ai[i*m+j] - di[j]*Ar[i*m+j];
	sjl = atan2(ajdi,ajdr);

	tmp = PI*GAMBAR*ajdi*x[j];
	for(k = 0;k < npe;k++){
	  vx[k] -= Gm[i*npe + k]*tmp;
	  if(sjl != 0){
	    tmp2 = PI*GAMBAR*tmp*x[j]/sjl;
	    /* S diagonal */
	    Sx[k*npe+k] += Gm[i*npe + k]*Gm[i*npe + k]*tmp2;
	    /* S off diagonal */
	    for(l = 0; l < k;l++){
	      tmp3 = Gm[i*npe + k]*Gm[i*npe + l]*tmp2;
	      Sx[l*npe+k] += tmp3;
	      Sx[k*npe+l] += tmp3;
	    }
	  }
	}

	tmp = PI*GAMBAR*ajdi*y[j];
	for(k = 0;k < npe;k++){
	  vy[k] -= Gm[i*npe + k]*tmp;
	  if(sjl != 0){
	    tmp2 = PI*GAMBAR*tmp*y[j]/sjl;
	    /* S diagonal */
	    Sy[k*npe+k] += Gm[i*npe + k]*Gm[i*npe + k]*tmp2;
	    /* S off diagonal */
	    for(l = 0; l < k;l++){
	      tmp3 = Gm[i*npe + k]*Gm[i*npe + l]*tmp2;
	      Sy[l*npe+k] += tmp3;
	      Sy[k*npe+l] += tmp3;
	    }
	  }
	}

	tmp = PI*GAMBAR*ajdi*z[j];
	for(k = 0;k < npe;k++){
	  vz[k] -= Gm[i*npe + k]*tmp;
	  if(sjl != 0){
	    tmp2 = PI*GAMBAR*tmp*z[j]/sjl;
	    /* S diagonal */
	    Sz[k*npe+k] += Gm[i*npe + k]*Gm[i*npe + k]*tmp2;
	    /* S off diagonal */
	    for(l = 0; l < k;l++){
	      tmp3 = Gm[i*npe + k]*Gm[i*npe + l]*tmp2;
	      Sz[l*npe+k] += tmp3;
	      Sz[k*npe+l] += tmp3;
	    }
	  }
	}

      }
    }
  }
  
  /* non-separable term */
  double ajai_i, ajai_r, sji;  

  for(i = 0;i < p->nt;i++){
    for(j = 0;j < i;j++){  
      if(p->nbeta[i] + p->nbeta[j] <= p->betath){

	/* loop over space */
	for(k = si;k < ei;k++){
	  ajai_r = -Ar[i*m+k]*Ar[j*m+k] - Ai[i*m+k]*Ai[j*m+k];
	  ajai_i = -Ar[i*m+k]*Ai[j*m+k] + Ai[i*m+k]*Ar[j*m+k];
	  
	  /* calculate phase angle */
	  sji = atan2(ajai_i,ajai_r);
	  
	  tmp = -PI*GAMBAR*x[k]*ajai_i;
	  for(l = 0;l < npe;l++){
	    vx[l] -= (Gm[i*npe + l] - Gm[j*npe + l])*tmp;
	    if(sji != 0){
	      tmp2 = -PI*GAMBAR*tmp*x[k]/sji;
	      /* S diagonal */
	      Sx[l*npe + l] += (Gm[i*npe + l] - Gm[j*npe + l])*(Gm[i*npe + l] - Gm[j*npe + l])*tmp2;
	      /* S off diagonal */
	      for(n = 0;n < l;n++){
		tmp3 = (Gm[i*npe + l] - Gm[j*npe + l])*(Gm[i*npe + n] - Gm[j*npe + n])*tmp2;
		Sx[l*npe + n] += tmp3;
		Sx[n*npe + l] += tmp3;
	      }
	    }
	  }
	  
	  tmp = -PI*GAMBAR*y[k]*ajai_i;
	  for(l = 0;l < npe;l++){
	    vy[l] -= (Gm[i*npe + l] - Gm[j*npe + l])*tmp;
	    if(sji != 0){
	      tmp2 = -PI*GAMBAR*tmp*y[k]/sji;
	      /* S diagonal */
	      Sy[l*npe + l] += (Gm[i*npe + l] - Gm[j*npe + l])*(Gm[i*npe + l] - Gm[j*npe + l])*tmp2;
	      /* S off diagonal */
	      for(n = 0;n < l;n++){
		tmp3 = (Gm[i*npe + l] - Gm[j*npe + l])*(Gm[i*npe + n] - Gm[j*npe + n])*tmp2;
		Sy[l*npe + n] += tmp3;
		Sy[n*npe + l] += tmp3;
	      }
	    }
	  }
	  
	  tmp = -PI*GAMBAR*z[k]*ajai_i;
	  for(l = 0;l < npe;l++){
	    vz[l] -= (Gm[i*npe + l] - Gm[j*npe + l])*tmp;
	    if(sji != 0){
	      tmp2 = -PI*GAMBAR*tmp*z[k]/sji;
	      /* S diagonal */
	      Sz[l*npe + l] += (Gm[i*npe + l] - Gm[j*npe + l])*(Gm[i*npe + l] - Gm[j*npe + l])*tmp2;
	      /* S off diagonal */
	      for(n = 0;n < l;n++){
		tmp3 = (Gm[i*npe + l] - Gm[j*npe + l])*(Gm[i*npe + n] - Gm[j*npe + n])*tmp2;
		Sz[l*npe + n] += tmp3;
		Sz[n*npe + l] += tmp3;
	      }
	    }
	  }
	  
	  
	}
      }
    }
  }
  
#ifndef NOTHREADS
  pthread_exit(NULL);
#endif
  
}

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] )
     
{ 
    double *Ar; 
    double *Ai;
    double *dr;
    double *di;
    double *Gm;
    double *nbeta;
    double *vx,*vy,*vz;
    double *Sx,*Sy,*Sz;
    double *xx;
    int m, npe, nt;
    int nthreads;
    int i,j,k;
    double betath;

#ifndef NOTHREADS
    pthread_t *threads;
#endif
    int rc;
    params *p;

    /* Check for proper number of arguments */
    
    if (nrhs != 7) { 
	mexErrMsgTxt("Seven input arguments required."); 
    } else if (nlhs > 6) {
	mexErrMsgTxt("Too many output arguments."); 
    } 

    /* Assign pointers to the various parameters */ 
    Ar = mxGetPr(A);
    Ai = mxGetPi(A);
    xx = mxGetPr(XX);
    dr = mxGetPr(D);
    di = mxGetPi(D);
    Gm = mxGetPr(GM);
    nbeta = mxGetPr(NB);
    betath = mxGetScalar(BTH);

    m = mxGetM(A);
    npe = mxGetM(GM);
    nt = mxGetN(A);

    nthreads = mxGetScalar(NT);
    int *nst = mxCalloc(nthreads,sizeof(int));
    for(i = 0;i < nthreads;i++) nst[i] = (int)(m/nthreads);
    if(m - ((int)(m/nthreads))*nthreads > 0) /* need to add remainder to last thread */
      nst[nthreads-1] += m - ((int)(m/nthreads))*nthreads;    

    /* Create matrices for the return arguments */ 
    VXOUT = mxCreateDoubleMatrix(npe, 1, mxREAL); 
    SXOUT = mxCreateDoubleMatrix(npe, npe, mxREAL);
    vx = mxGetPr(VXOUT);
    Sx = mxGetPr(SXOUT);
    VYOUT = mxCreateDoubleMatrix(npe, 1, mxREAL); 
    SYOUT = mxCreateDoubleMatrix(npe, npe, mxREAL);
    vy = mxGetPr(VYOUT);
    Sy = mxGetPr(SYOUT);
    VZOUT = mxCreateDoubleMatrix(npe, 1, mxREAL); 
    SZOUT = mxCreateDoubleMatrix(npe, npe, mxREAL);
    vz = mxGetPr(VZOUT);
    Sz = mxGetPr(SZOUT);

    /* Build param structs for each thread */
    p = mxCalloc(nthreads,sizeof(params));
    int si = 0;
    for(i = 0;i < nthreads;i++){
      
      /* scalars */
      p[i].m = m;
      p[i].npe = npe;
      p[i].nt = nt;
      p[i].betath = betath;

      /* vectors and matrices */
      p[i].nbeta = mxCalloc(nt,sizeof(double)); /* check that this is really the length of nbeta!! */
      for(j = 0;j < nt;j++) p[i].nbeta[j] = nbeta[j];
      p[i].Gm = mxCalloc(nt*npe,sizeof(double));
      for(j = 0;j < nt*npe;j++) p[i].Gm[j] = Gm[j];

      /* spatial locs to work on */
      p[i].si = si;
      p[i].ei = si+nst[i];
      si += nst[i];

      /* pointers */
      p[i].Ar = Ar;
      p[i].Ai = Ai;
      p[i].dr = dr;
      p[i].di = di;
      p[i].x = xx;
      p[i].y = &xx[m];
      p[i].z = &xx[2*m];
      
      /* solutions */
      p[i].vx = mxCalloc(npe,sizeof(double));
      p[i].Sx = mxCalloc(npe*npe,sizeof(double));
      p[i].vy = mxCalloc(npe,sizeof(double));
      p[i].Sy = mxCalloc(npe*npe,sizeof(double));
      p[i].vz = mxCalloc(npe,sizeof(double));
      p[i].Sz = mxCalloc(npe*npe,sizeof(double));

    }

#ifndef NOTHREADS
    threads = mxCalloc(nthreads,sizeof(pthread_t));
    for(i = 0;i < nthreads;i++){
      rc = pthread_create(&threads[i], NULL, docalcvS_lt, (void *)&p[i]);
      if (rc){
	mexErrMsgTxt("problem with return code from pthread_create()");
      }
    }

    /* wait for all threads to finish */
    for(i = 0;i < nthreads;i++){
      pthread_join(threads[i],NULL);
    }
#else
    /* process them serially */
    for(i = 0;i < nthreads;i++) docalcvS_lt((void *)&p[i]);
#endif

    /* sum results */
    for(i=0;i < npe;i++){
      for(j=0;j < nthreads;j++){
	vx[i] += p[j].vx[i];
	vy[i] += p[j].vy[i];
	vz[i] += p[j].vz[i];
      }
    }
    for(i=0;i < npe*npe;i++){
      for(j=0;j < nthreads;j++){
	Sx[i] += p[j].Sx[i];
	Sy[i] += p[j].Sy[i];
	Sz[i] += p[j].Sz[i];
      }
    }    

    /* free memory */
    for(i=0;i < nthreads;i++){
      mxFree(p[i].vx);
      mxFree(p[i].Sx);
      mxFree(p[i].vy);
      mxFree(p[i].Sy);
      mxFree(p[i].vz);
      mxFree(p[i].Sz);
      mxFree(p[i].nbeta);
      mxFree(p[i].Gm);
    }
    mxFree(p);
#ifndef NOTHREADS
    mxFree(threads);
#endif
    mxFree(nst);

    return;
    
}
