function [a,b,alpha,err] = cal_dRF(brfvec,ntbrf,rf,gradrf,d,dt,gam,sens,garea,xx,f0,fb,nthreads,a0,adphs,b0,bdphs,cost,beta,computemethod);
  
  % get search direction (derivatives)
  Nrungs = size(garea,1);
  Nc = size(sens,2); % number of tx channels
  Nb = 1;
  Ns = size(xx,1); % number of spatial locations
  
  % line search to get step
  costt = cost(end);
  tl = 1;
  al = 0.3; bl = 0.3;
  while (costt > cost(end) + al*tl*real(gradrf'*d))
    
    % reduce t
    tl = bl*tl;
    
    % get test point
    rft = rf + tl*d;
    
    % simulate
    at = zeros(Ns,Nb);
    bt = zeros(Ns,Nb);
    rfmat = reshape(rft,[Nrungs Nc]);
    switch(computemethod)
        case 'mex'
            [at,bt] = blochsim_optcont_mex(brfvec,ntbrf,rfmat*dt*gam,complexify(sens),garea*gam,xx,2*pi*f0*dt,nthreads);
            
        otherwise
            error 'Unrecognized compute method'
    end;
    
    % calculate cost
    ea = col(at - adphs);
    eb = col(bt - bdphs);
    costt = 1/2*(ea'*ea + eb'*eb) + 1/2*beta*(rft'*rft);   
  end
  
  if tl ~= 1
      a = at; b = bt;
      alpha = tl;
      err = costt;
  else
      a = a0; b = b0;
      alpha = 0;
      err = costt;
  end;
end