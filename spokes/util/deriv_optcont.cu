__global__
void deriv_optcont( double *dr,
		    double *di,
		    double *afr,
		    double *afi,
		    double *bfr,
		    double *bfi,
		    double *auxar,
		    double *auxai,
		    double *auxbr,
		    double *auxbi,
		    double *sensr,
		    double *sensi,
		    double *omdt,
		    double *rfwr,
		    double *rfwi,
		    double *brfr,
		    double *brfi,
		    double *x,
		    double *y,
		    double *z,
		    double *gareax,
		    double *gareay,
		    double *gareaz,
		    int *ntrf,
		    const int ns,
		    const int nr,
		    const int cmplxbrf,
		    const int nc)		    
{

  /* get spatial index */
  int i = blockIdx.x*blockDim.x + threadIdx.x;

  int j,k,l; /* counters */
  double afrt,afit,bfrt,bfit;
  double arr,ari,brr,bri;
  double sensrfr,sensrfi;
  double art,ait,brt,bit;
  double zomr,zomi,zgr,zgi;
  double gtotal;
  double b1r,b1i,b1m;
  double c,s,sr,si;
  double da1r,da1i,da2r,da2i,db1r,db1i,db2r,db2i;
  double auxart,auxait,auxbrt,auxbit;
  double tmpr,tmpi;
  int brfc;
  int nttotal;

  int dind; /* index into derivative arrays */
  
  /* get total num of time points in the pulse */
  nttotal = 0;
  for(j = 0;j < nr-1;j++)
    nttotal += ntrf[j];

  while(i < ns){
    
    /* initialize reverse a, b */
    arr = 1;ari = 0;brr = 0;bri = 0;

    afrt = afr[i];afit = afi[i];
    bfrt = bfr[i];bfit = bfi[i];

    zomr = cos(omdt[i]/2);
    zomi = sin(omdt[i]/2);

    auxart = auxar[i];
    auxait = auxai[i];
    auxbrt = auxbr[i];
    auxbit = auxbi[i];

    brfc = nttotal;

    for(j = nr-1;j > -1;j--){ /* loop backwards over rungs */

      /* calculate gradient rotation */
      gtotal = x[i]*gareax[j] + y[i]*gareay[j] + z[i]*gareaz[j];
      zgr = cos(gtotal/2);
      zgi = sin(gtotal/2);

      /* strip off gradient blip from forward sim */
      art = afrt*zgr + afit*zgi;
      ait = afit*zgr - afrt*zgi;
      afrt = art;afit = ait;
      brt = bfrt*zgr - bfit*zgi;
      bit = bfit*zgr + bfrt*zgi;
      bfrt = brt;bfit = bit;	            

      /* add gradient blip to backward sim */
      art = arr*zgr - ari*zgi;
      ait = ari*zgr + arr*zgi;
      arr = art;ari = ait;
      brt = brr*zgr - bri*zgi;
      bit = bri*zgr + brr*zgi;
      brr = brt;bri = bit;	      

      /* get shimmed b1 for this rung */
      sensrfr = 0;sensrfi = 0;
      for(k = 0;k < nc;k++){
	sensrfr += sensr[i+k*ns]*rfwr[j+k*nr] - sensi[i+k*ns]*rfwi[j+k*nr];
	sensrfi += sensi[i+k*ns]*rfwr[j+k*nr] + sensr[i+k*ns]*rfwi[j+k*nr];
      }

      for(k = ntrf[j]-1;k > -1;k--){ /* loop over subpulse time */

	/* strip off-resonance from forward sim */
	art = afrt*zomr + afit*zomi;
	ait = afit*zomr - afrt*zomi;
	afrt = art;afit = ait;
	brt = bfrt*zomr - bfit*zomi;
	bit = bfit*zomr + bfrt*zomi;
	bfrt = brt;bfit = bit;	

	/* add off-resonance to backward sim */
	art = arr*zomr - ari*zomi;
	ait = ari*zomr + arr*zomi;
	arr = art;ari = ait;
	brt = brr*zomr - bri*zomi;
	bit = bri*zomr + brr*zomi;
	brr = brt;bri = bit;	

	/* weight rf by subpulse */
	b1r = brfr[brfc+k]*sensrfr; 
	b1i = brfr[brfc+k]*sensrfi;
	if(cmplxbrf == 1){
	  b1r -= brfi[brfc+k]*sensrfi;
	  b1i += brfi[brfc+k]*sensrfr;
	}
	    
	/* calculate RF rotation params */
	b1m = sqrt(b1r*b1r + b1i*b1i);
	c = cos(b1m/2);
	s = sin(b1m/2);
	if(b1m > 0){
	  sr = -s*b1i/b1m;
	  si = s*b1r/b1m;
	}else{
	  sr = 0;si = 0;
	}

	/* strip off current rf rotation from forward sim */
	art = afrt*c + bfrt*sr + bfit*si;
	ait = afit*c + bfit*sr - bfrt*si;
	brt = -afrt*sr + afit*si + bfrt*c;
	bit = -afrt*si - afit*sr + bfit*c;
	afrt = art;afit = ait;
	bfrt = brt;bfit = bit;

	/* calculate derivatives (incomplete) */ 
	tmpr = -bfrt*ari - bfit*arr;
	tmpi = -bfrt*arr + bfit*ari;
	da1r = tmpr*auxart - tmpi*auxait;
	da1i = tmpr*auxait + tmpi*auxart;
	tmpr = -afrt*bri + afit*brr;
	tmpi = afrt*brr + afit*bri;
	da2r = tmpr*auxart - tmpi*auxait;
	da2i = tmpr*auxait + tmpi*auxart;
	tmpr = -bri*bfrt - brr*bfit;
	tmpi = -brr*bfrt + bri*bfit;
	db1r = tmpr*auxbrt - tmpi*auxbit;
	db1i = tmpr*auxbit + tmpi*auxbrt;
	tmpr = afrt*ari - afit*arr;
	tmpi = -afrt*arr - afit*ari;
	db2r = tmpr*auxbrt - tmpi*auxbit;
	db2i = tmpr*auxbit + tmpi*auxbrt;
	/* loop through coils to calculate derivatives */
	for(l = 0;l < nc;l++){ /* l: coil index, i: spatial index, j: rung index, k: subpulse time index */
	  dind = l*nc*ns + j*ns + i;
	  tmpr = (da2r + db2r + da1r + db1r)*sensr[i+l*ns] + (da2i + db2i - da1i - db1i)*sensi[i+l*ns];
	  tmpi = -(da2r + db2r + da1r + db1r)*sensi[i+l*ns] + (da2i + db2i - da1i - db1i)*sensr[i+l*ns];
	  dr[dind] += brfr[brfc+k]*tmpr/2;
	  di[dind] += brfr[brfc+k]*tmpi/2;
	  if(cmplxbrf == 1){
	    dr[dind] += brfi[brfc+k]*tmpi/2;
	    di[dind] -= brfi[brfc+k]*tmpr/2;
	  }	  
	}	

	/* add current rf rotation to backward sim */
	art = arr*c - brr*sr - bri*si;
	ait = ari*c + bri*sr - brr*si;
	brt = arr*sr + ari*si + brr*c;
	bit = arr*si - ari*sr + bri*c;
	arr = art;ari = ait;
	brr = brt;bri = bit;
	
      } /* loop over subpulse time */
      
      if(j > 0)
	brfc -= ntrf[j-1];

    } /* loop backwards over rungs */

    i += blockDim.x * gridDim.x;

  } /* loop over spatial locs */

}
