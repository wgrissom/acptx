function [rf,kpe,a,b,beta,phsa,psba,phsb,psbb,cost,compWts] = lt_oc_zc(ad,bd,sens,xx,kinit,f0,fb,rf,prbp,algp,gpos,gfly,rampsamp,nramp)
disp('Designing large-tip pulses.')

gambar = 4257;             % gamma/2pi in Hz/T
gam = gambar*2*pi;         % gamma in radians/g

% calculate area of phase encodes from kpe
garea = diff([kinit;zeros(1,size(kinit,2))],1)/gambar;

Nrungs = size(garea,1);

Nc = size(sens,2); % number of tx channels
Ns = size(xx,1); % number of spatial locations
Nb = length(fb); % number of frequency bands

compWts = eye(Nc); % compression weights

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF basis for lt design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if prbp.z_tbw > 0
%   [Brf,avgflip,rf] = genRFbasis(Nrungs,prbp.dt,rfss,sens,rf,prbp.d1,prbp.d2, ...
%                                 prbp.z_tbw,rampsamp,gpos,nramp,gfly);
%   
%   % convert Brf to vector for mex blochsim
%   brfvec = [];
%   for ii = 1:length(Brf)
%     brfvec = [brfvec;Brf{ii}(:)];
%     ntbrf(ii) = length(Brf{ii});
%   end
% else % kT-points - the subpulses won't change
%   % convert Brf to vector for mex blochsim
%   brfvec = [];
%   for ii = 1:length(rfss)
%     brfvec = [brfvec;rfss{ii}];
%     ntbrf(ii) = length(rfss{ii});
%   end
%   avgflip = [];
%   Brf = rfss;
% end

prbp.brfvec = [];
for ii = 1:length(prbp.rfss)
    prbp.brfvec = [prbp.brfvec; prbp.rfss{ii}];
    prbp.ntbrf(ii) = length(prbp.rfss{ii});
end;

% get relationship between phase encodes and poly terms
[Ag,Bg,Ans,Bns] = get_ab_to_g(Nrungs);

% target phase variables and phase-modified patterns

  switch algp.phsopt
   case 'joint'
    phsa = zeros(Ns,1);psba = zeros(Nb-1,1);
    phsb = zeros(Ns,1);psbb = zeros(Nb-1,1);
   case {'independent','none'}
    phsa = zeros(Ns,Nb);psba = [];
    phsb = zeros(Ns,Nb);psbb = [];
  end
  adphs = ad;
  bdphs = bd;


% algorithm switches/counters and error vector
cost = [Inf Inf];
mincost = Inf;

while ((cost(end) < algp.maintol*cost(end-1)) || (length(cost)-2 <= algp.mainMinIters)) && (length(cost)-2 <= algp.mainMaxIters)
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Build new object with latest gradients, RF basis
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % Bloch sim with new rf basis
  a = zeros(Ns,Nb);
  b = zeros(Ns,Nb);
  rfmat = reshape(rf,[Nrungs Nc]);
  switch(algp.compmethod)
   case 'dotm'
    for ii = 1:Nb
      [a(:,ii),b(:,ii)] = blochsim_optcont(prbp.rfss,rfmat*prbp.dt*gam,sens,garea*gam,xx,2*pi*(f0+fb(ii))*dt);
    end
   case 'mex'
    for ii = 1:Nb
      [a(:,ii),b(:,ii)] = blochsim_optcont_mex(prbp.brfvec,prbp.ntbrf,rfmat*prbp.dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*prbp.dt,algp.nthreads);
    end
   case 'gpu'
    for ii = 1:Nb
      [a(:,ii),b(:,ii)] = blochsim_optcont_cuda(sens,2*pi*(f0+fb(ii))*prbp.dt,rfmat*prbp.dt*gam,prbp.brfvec,xx,garea*gam,prbp.ntbrf);
    end
   otherwise
    error 'Unrecognized compute method'
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % update target excitation phase
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  switch algp.phsopt
   case 'joint'
    if any(abs(ad) > 0) % ad = 0 if 180 so target phase doesnt matter
      [phsa,psba] = phase_update(ad,a,phsa,psba,5);
    end
    [phsb,psbb] = phase_update(bd,b,phsb,psbb,5);
    % apply shifted phase to desired excitation patterns
    adphs(:,1) = ad(:,1).*exp(1i*phsa);
    bdphs(:,1) = bd(:,1).*exp(1i*phsb);
    for ii = 1:Nb-1
      adphs(:,ii+1) = ad(:,ii+1).*exp(1i*(phsa+psba(ii)));
      bdphs(:,ii+1) = bd(:,ii+1).*exp(1i*(phsb+psbb(ii)));
    end
   case 'independent'
    if any(abs(ad) > 0)
      phsa = angle(a);
    end
    phsb = angle(b);
    % apply shifted phase to desired excitation patterns
    adphs = ad.*exp(1i*phsa);
    bdphs = bd.*exp(1i*phsb);
   case 'none'
    if length(cost)-2 == 0
      if any(abs(ad) > 0)
        phsa = angle(a);
      end
      phsb = angle(b);
      % apply shifted phase to desired excitation patterns
      adphs = ad.*exp(1i*phsa);
      bdphs = bd.*exp(1i*phsb);
    end
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % calculate error
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ea = col(a - adphs);
  eb = col(b - bdphs);
  cost(end+1) = 1/2*real(ea'*ea + eb'*eb);
  RFreg = 1/2*prbp.beta*real(rf'*rf);
  
  fprintf('Iter %d. Mean flip angle: %0.4f degrees. Flip angle RMSE: %0.4f degrees. Peak RF: %0.2f.\n',...
      length(cost)-2,mean(abs(2*asin(abs(b(:)))))*180/pi, sqrt(mean(abs(2*asin(abs(b(:))))*180/pi - prbp.delta_tip).^2), max(abs(rf)));
  
  % check to make sure that RF regularization is big enough
  if (rem(length(cost)-2,10) == 0) && ...
        ((RFreg > 1.25*cost(end)) || (RFreg < 0.75*cost(end))) ...
        && (cost(end) < 1.25*mincost) && prbp.betaadjust
    disp('Adjusting RF penalty.');
    beta = cost(end)/RFreg*prbp.beta;
    mincost = min([cost mincost]);
    cost = [inf cost(end)+1/2*prbp.beta*real(rf'*rf)];
  else
    cost(end) = cost(end) + RFreg;
  end;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % update pulses
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
%   [rf,a,b] = rfupdate(rf,garea,gam,xx,a,b,adphs,bdphs,sens,f0,prbp,algp);

  % get search direction (derivatives)
  switch(algp.compmethod)
   case 'mex'
    for ii = 1:Nb
      drf(:,ii) = deriv_optcont_mex(prbp.brfvec,prbp.ntbrf,rfmat*prbp.dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*prbp.dt,algp.nthreads,(a(:,ii)-adphs(:,ii)),(b(:,ii)-bdphs(:,ii)),a(:,ii),b(:,ii));
    end   
   case 'gpu'
    for ii = 1:Nb
      drf(:,ii) = deriv_optcont_cuda(sens,2*pi*(f0+fb(ii))*dt,rfmat*dt*gam,prbp.brfvec,xx,garea*gam,prbp.ntbrf,(a(:,ii)-adphs(:,ii)),(b(:,ii)-bdphs(:,ii)),a(:,ii),b(:,ii));
    end
   otherwise
    error 'Unrecognized compute method'
  end
  
  % sum across freq bands
  drf = prbp.dt*gam*sum(drf,2);
  % add power penalty derivative
  drf = drf + prbp.beta*rf;
  
  % line search to get step
  costt = cost(end);
  tl = 1;
  al = 0.3; bl = 0.3;
  while (costt > cost(end) - al*tl*real(drf'*drf))
    
    % get test point
    rft = rf - tl*drf;
    
    % simulate
    at = zeros(Ns,Nb);
    bt = zeros(Ns,Nb);
    rfmat = reshape(rft,[Nrungs Nc]);
    switch(algp.compmethod)
     case 'dotm'
      for ii = 1:Nb
        [at(:,ii),bt(:,ii)] = blochsim_optcont(prbp.brfvec,rfmat*prbp.dt*gam,sens,garea*gam,xx,2*pi*(f0+fb(ii))*dt);
      end
     case 'mex'
      for ii = 1:Nb
        [at(:,ii),bt(:,ii)] = blochsim_optcont_mex(prbp.brfvec,prbp.ntbrf,rfmat*prbp.dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*prbp.dt,algp.nthreads);
      end
     case 'gpu'
      for ii = 1:Nb
        [at(:,ii),bt(:,ii)] = blochsim_optcont_cuda(sens,2*pi*(f0+fb(ii))*prbp.dt,rfmat*prbp.dt*gam,prbp.brfvec,xx,garea*gam,prbp.ntbrf);
      end
     otherwise
      error 'Unrecognized compute method'
    end
    
    % calculate cost
    ea = col(at - adphs);
    eb = col(bt - bdphs);
    costt = 1/2*real(ea'*ea + eb'*eb) + 1/2*prbp.beta*real(rft'*rft);
    
    % reduce t
    tl = bl*tl;
    
  end
  rf = rft;
  a = at;b = bt;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % compress the RF, re-simulate
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  if Nrungs > prbp.Ncred
      
      rfw = reshape(rf,[Nrungs Nc]);
      % SVT Compression
      [u,s,v] = svd(rfw.','econ');
      s(prbp.Ncred+1:end,prbp.Ncred+1:end) = 0; % Hard thresholding
      rfw = (u*(s*v')).';rf = rfw(:);
      compWts = u(:,1:prbp.Ncred); % compression weights
      
      % simulate
      a = zeros(Ns,Nb);
      b = zeros(Ns,Nb);
      rfmat = reshape(rf,[Nrungs Nc]);
      switch(algp.compmethod)
          case 'dotm'
              for ii = 1:Nb
                  [a(:,ii),b(:,ii)] = blochsim_optcont(prbp.rfss,rfmat*prbp.dt*gam,sens,garea*gam,xx,2*pi*(f0+fb(ii))*prbp.dt);
              end
          case 'mex'
              for ii = 1:Nb
                  [a(:,ii),b(:,ii)] = blochsim_optcont_mex(prbp.brfvec,prbp.ntbrf,rfmat*prbp.dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*prbp.dt,algp.nthreads);
              end
          case 'gpu'
              for ii = 1:Nb
                  [a(:,ii),b(:,ii)] = blochsim_optcont_cuda(sens,2*pi*(f0+fb(ii))*prbp.dt,rfmat*prbp.dt*gam,prbp.brfvec,xx,garea*gam,prbp.ntbrf);
              end
          otherwise
              error 'Unrecognized compute method'
      end;
      
  end;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%
  % update gradients
  %%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  [garea,a,b] = gradupdate(rf,sens,garea,gam,xx,f0,fb,a,b,adphs,bdphs,prbp,algp);

end

% calculate final phase encode locations from gradient blip areas
kpe = -gambar * flipud(cumsum(flipud(garea),1));

% balance RF regularization
beta = (cost(end)-RFreg)/RFreg*prbp.beta;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function to generate RF basis using SLR 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Brf,avgflip,rf] = genRFbasis(Nrungs,dt,Brf,sens,rf,d1,d2,z_tbw,rampsamp,gpos,nramp,gfly)

rf = reshape(rf,[Nrungs length(rf)/Nrungs]);

Ns = size(sens,1);

gambar = 4257;             % gamma/2pi in Hz/T
gam = gambar*2*pi;         % gamma in radians/g

% determine avg flip for each rung
avgflip = [];
for ii = 1:Nrungs
  if ~iscell(Brf)
    sssum = abs(sum(Brf));
  else
    sssum = abs(sum(Brf{ii}));
  end
  avgflip(ii) = gam*dt*sssum*sum(abs(sens*rf(ii,:).'));
end
avgflip = avgflip/Ns;

% design slr pulses to hit these flips at isocenter
for ii = 1:Nrungs
  bsf = sin(avgflip(ii)/2);
  np = 128; 
  rfslr = slr(np,z_tbw,d1,d2,bsf);
  %bslr = bsf*dzls(np,z_tbw,d1,d2);
  %rfslr = real(col(b2rf(bslr)));
  if rampsamp
    % verse the pulse onto the actual trajectory
    rfslr = [verse(gpos{ii},rfslr);zeros(length(gfly{ii}),1)];
  else
    % no verse-ing, just add zeros to account for attack and decay
    rfslr = [zeros(nramp(ii),1);verse(gpos{ii}(nramp(ii)+1:end-nramp(ii)),rfslr);zeros(nramp(ii),1);zeros(length(gfly{ii}),1)];
  end
  Brfnew{ii} = rfslr(:)/max(abs(rfslr));
  % adjust rf weights to achieve same flip in slice center with the
  % new pulse
  if ~iscell(Brf)
    rf(ii,:) = rf(ii,:)*sum(Brf)/sum(Brfnew{ii});
  else
    rf(ii,:) = rf(ii,:)*sum(Brf{ii})/sum(Brfnew{ii});
  end

end

Brf = Brfnew;
rf = rf(:);
