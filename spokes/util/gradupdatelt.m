function [a,b,garea] = gradupdatelt(Brf,rfw,sens,garea0,xx,f0,fb,dt,mask,a0,b0,adphs,bdphs,algp)


    % Inputs:
    % Brf     {Nrungs} - basis slice-selective waveforms for each rung
    % rfw     [Nrungs Nc] - rf weights for each rung and each coil
    % sens    [Ns Nc] - tx sensitivities for each coil
    % omdt    [Ns 1]  - off-resonance map
    
    Nc = size(sens,2); % number of tx channels
    Ns = size(xx,1); % number of spatial locations
    Nb = length(fb); % number of frequency bands

    gambar = 4257;             % gamma/2pi in Hz/T
    gam = gambar*2*pi;         % gamma in radians/g

    %% Part 1: Calculate a,b for each subpulse

    % convert Brf to vector for mex blochsim
    Nrungs = length(Brf);
    brfvec = Brf;
    for ii = 1:Nrungs
        ntbrf(ii) = length(Brf{ii});
    end
    
    rfmat = reshape(rfw,[Nrungs Nc]);
    
    Arf0 = cell(1,Nb); Brf0 = cell(1,Nb);
    for jj = 1:Nb
        Arf0{jj} = zeros(Ns,Nrungs); Brf0{jj} = zeros(Ns,Nrungs);
        for ii = 1:Nrungs
            % Assuming Nb = 1 for now.
            [Arf0{jj}(:,ii), Brf0{jj}(:,ii)] = blochsim_optcont_mex(brfvec{ii}(:),ntbrf(ii),rfmat(ii,:)*dt*gam,complexify(sens),0.*xx*gam,xx,2*pi*(f0+fb(jj))*dt,algp.nthreads);
        end;
    end;
        
    a = a0;
    b = b0;
    nn = 1; errOld = Inf; eps = Inf;
    while ((nn <= algp.maxcgiters && eps > algp.cgstopthresh) || rem((nn-1),10) == 1)
        
        if rem(nn,10) ~= 1
            garea0 = garea;
        end;
                
        % CG
        if rem(nn,10) ~= 1
            gradgareaold = gradgarea;
        end;
         
        auxA = a - adphs;
        auxB = b - bdphs;

        ar = ones(Ns,Nb); br = zeros(Ns,Nb);
        af = a; bf = b;

        % keyboard
        gradgareaX = zeros(Nrungs,1);
        gradgareaY = zeros(Nrungs,1);
        for jj = Nrungs:-1:1
            % Strip off current gradient rotation from forward sim.
            zg = exp(1i/2*xx*gam*garea0(jj,:)');
            
            for ii = 1:Nb                
                
                % Calculate the deriv of current gradient rotation
                dA = ar(:,ii) .* af(:,ii) + conj(br(:,ii)) .* bf(:,ii);
                dB = br(:,ii) .* af(:,ii) - conj(ar(:,ii)) .* bf(:,ii);
                
                gradgareaX(jj) = gradgareaX(jj) + gam/2* real( (1i * dA .* xx(:,1))' * auxA(:,ii) + (1i * dB .* xx(:,1))' * auxB(:,ii) );
                gradgareaY(jj) = gradgareaY(jj) + gam/2* real( (1i * dA .* xx(:,2))' * auxA(:,ii) + (1i * dB .* xx(:,2))' * auxB(:,ii) );
            
                % Add current gradient rotation to backward sim.
                ar(:,ii) = ar(:,ii) .* zg; br(:,ii) = br(:,ii) .* zg;
            
                % Strip off current rf rotation from forward sim
                aft =  af(:,ii) .* conj(Arf0{ii}(:,jj)) + bf(:,ii) .* conj(Brf0{ii}(:,jj));
                bft = -af(:,ii) .*      Brf0{ii}(:,jj)  + bf(:,ii) .*     (Arf0{ii}(:,jj));
                af(:,ii) = aft; bf(:,ii) = bft;

                % Add current rf rotation to backward sim
                art = ar(:,ii) .* Arf0{ii}(:,jj) - conj(br(:,ii)) .* Brf0{ii}(:,jj);
                brt = br(:,ii) .* Arf0{ii}(:,jj) + conj(ar(:,ii)) .* Brf0{ii}(:,jj);
                ar(:,ii) = art; br(:,ii) = brt;
            end;

        end;
        gradgarea = [gradgareaX, gradgareaY]; % last term always 0? % need further debug
               
        %% Part 3: Calculate Step Size for gradient update

        % CG
        if rem(nn,10) == 1 % Multi-start
        % if nn == 1
            d = -gradgarea;
        else
            % need check
            betaGrad = (gradgarea(:)'*(gradgarea(:)-gradgareaold(:)))/(gradgareaold(:)'*gradgareaold(:));
            d = -gradgarea + betaGrad*d;
        end;
        
        for ii=1:Nb
            [a(:,ii),b(:,ii)] = garea_to_ab(Arf0{ii},Brf0{ii},gam*garea0,xx);
        end;

        % calculate cost
        ea = col(a - adphs);
        eb = col(b - bdphs);
        cost = 1/2*(ea'*ea + eb'*eb);
            
costt = cost;
al = algp.gradal; bl = algp.gradbl; tl = 1/bl;
while (costt > cost + al*tl*real(gradgarea(:)'*d(:))) && tl > algp.gradmintl

            tl = tl * bl;
                        
            gareat = garea0 + tl*d;

            for ii=1:Nb
                [a(:,ii),b(:,ii)] = garea_to_ab(Arf0{ii},Brf0{ii},gam*gareat,xx);
            end;
            
            % calculate cost
            ea = col(a - adphs);
            eb = col(b - bdphs);
            costt = 1/2*(ea'*ea + eb'*eb);
            
end;

        % Output result (for case when the above while loop did not run)
        if tl == 1/bl
            garea = garea0;
            for ii=1:Nb
                [a(:,ii),b(:,ii)] = garea_to_ab(Arf0{ii},Brf0{ii},gam*garea,xx);
            end;
        else
            garea = gareat;
        end;
        
        % debug
        ea = col(a - adphs);
        eb = col(b - bdphs);
        errNew = 1/2*(ea'*ea + eb'*eb);
        
        eps = (errOld - errNew) / errNew;
        errOld = errNew;
        
%         fprintf('Grad Update - Itr: %d, Cost: %0.4f.\n',nn,cost);
            
        nn = nn + 1;
        
    end;
end