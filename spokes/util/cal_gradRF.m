function gradRF = cal_gradRF(brfvec,ntbrf,rf,dt,gam,sens,garea,xx,f0,fb,nthreads,a,adphs,b,bdphs,beta,computemethod)
  
  % get search direction (derivatives)
  Nrungs = size(garea,1);
  Nc = size(sens,2); % number of tx channels

  Nb = length(fb);
  rfmat = reshape(rf,[Nrungs Nc]);
  
  switch(computemethod)
   case 'mex'
       for ii = 1:Nb
           gradRF(:,ii) = deriv_optcont_mex(brfvec,ntbrf,rfmat*dt*gam,complexify(sens),...
               garea*gam,xx,2*pi*(f0+fb(ii))*dt,nthreads,complexify(a(:,ii)-adphs(:,ii)),...
               complexify(b(:,ii)-bdphs(:,ii)),complexify(a(:,ii)),complexify(b(:,ii)));
       end;
       
   otherwise
    error 'Unrecognized compute method'
  end;
  
  % sum across freq bands
  gradRF = dt*gam*sum(gradRF,2);
  % add power penalty derivative
  gradRF = gradRF + beta*rf;
end