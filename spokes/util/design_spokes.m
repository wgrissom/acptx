function [all_images, waveforms, aout, bout, err, nSVT] = design_spokes(prbp, algp, maps, rf, kpe, ain, bin)

fov = maps.fov; % Field of View, cm
dt = prbp.dt; % sampling period, seconds
dimxy = prbp.dimxy; % pixels, dim of design grid
Ns = prod(dimxy); % total # pixels
delta_tip = prbp.delta_tip; % flip angle, degrees

if delta_tip > 180 || delta_tip < 1
  error('Please specify a tip angle between 1 and 180 degrees');
end

flyback = prbp.flyback; % Use a flyback gradient trajectory
sameflyback = prbp.sameflyback;
Nrungs = prbp.Nrungs; % Number of rungs/spokes

if Nrungs == 1
  flyback = 0; % flyback irrelevant if only one spoke
  ltgradupdate = 0; % irrelevant if only one spoke (no x-y grad blips)
end

if abs(rem(Nrungs,1)) > 0 || Nrungs <= 0
  error('Number of spokes must be a positive integer');
end

z_tbw = prbp.z_tbw; % Time-Bandwidth product of slice select pulse
if abs(rem(z_tbw,2)) > 0 || z_tbw <= 0
  error('Time-bandwidth must be an even positive integer');
end

z_pulsetype = prbp.z_pulsetype;

dthick = prbp.dthick/10; % slice thickness (cm)
if dthick <= 0
  error('Slice thickness must be positive');
end

if prbp.gmax <= 0
  error('Max z gradient must be positive');
end

if prbp.maxpulsedur <= 0
  error('Max pulse duration must be greater than zero');
end

gmaxsys = prbp.maxgradamp;      % maximum grad amp (g/cm)
gslew = prbp.maxgradslew;       % maximum slew rate (g/cm/s)
filtertype = prbp.filtertype;   % spectral filter type (for multiband designs)

if prbp.rflim < 0
  error('RF limit must be greater than zero');
end

if prbp.trajres <= 0
  error('Pulse Spatial Resolution must be positive');
end

% Single Band Design
fb = 0;
Nb = 1;

large_tip_flag = delta_tip > 45;
if large_tip_flag
  disp('Doing a large-tip-angle spokes pulse design.');
end

gambar = 4257;             % gamma/2pi in Hz/g

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Large tip settings

Nc = size(maps.b1,3); % number of channels

% load B1 maps
B1in = repmat(maps.mask,[1 1 Nc]).*maps.b1;

% in case its not already done, subtract off first coil's phase
B1in = B1in.*exp(-1i*repmat(angle(B1in(:,:,1)),[1 1 Nc]));

fmap = maps.mask.*maps.b0;
sens = B1in; % B1int;
mask = maps.mask; % maskint;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get slice-select trapezoids
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

kzw = z_tbw / dthick;
gz_area = kzw / gambar; % slice select trap area (g/cm*sec) 

[gpos,nramp] = domintrap(gz_area,prbp.gmax,gslew,dt);
gpos = gpos(:);
gposnew = cell(Nrungs,1);
for ii = 1:Nrungs
  gposnew{ii} = gpos;
end
gpos = gposnew;
nramp = nramp*ones(Nrungs,1);

if flyback
  % no RF on flyback trap, so ramp sampling is ok
  gposarea = sum(gpos{end})*dt; % bc if no ramp sampling, this is ~= gz_area
  if ~sameflyback
    gfly = -dotrap(gposarea,gmaxsys,gslew,dt);
    gfly = gfly(:);
  else
    gfly = -gpos{1};
  end
else
  gfly = [];
end
gflynew = cell(Nrungs,1);
for ii = 1:Nrungs
  gflynew{ii} = gfly;
end
gfly = gflynew;
Ntz = length(gpos{end})+length(gfly{end});
Ntdes = Nrungs*Ntz; % total duration of pulse without final rewinder
pulsedur = Ntdes*dt;

if pulsedur*1000 > prbp.maxpulsedur
  error('Pulse duration (%0.2f ms) longer than maximum (%0.2f ms).',pulsedur*1000,prbp.maxpulsedur);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% design a slice-selective pulse
% on this gradient to init basis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bsf = sin(pi/12); % to get a small-tip pulse
rfslr = slr(prbp.np,z_tbw,prbp.d1,prbp.d2,bsf,z_pulsetype);
rfslr = real(col(rfslr));

% add zeros to account for attack and decay
b1d = [zeros(nramp(end),1); verse(gpos{end}(nramp(end)+1:end-nramp(end)),rfslr); zeros(nramp(end),1); zeros(length(gfly{end}),1)];

% normalize so our weights are in digital units
b1d = b1d/max(abs(b1d)); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get desired pattern for design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[xx,yy]=ndgrid(-fov/2:fov/dimxy:fov/2-fov/dimxy);

% desired excitation pattern
d = ones(Ns,Nb);

% apply shimmed phase
% d = d.*repmat(exp(1i*angle(col(sum(sens,3)))),[1 Nb]);
% calculate this here to use in solo init
phsshim = repmat(maps.phsinit,[1 Nb]);
%phsshim = repmat(exp(1i*angle(col(sum(sens,3)))),[1 Nb]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mask b1 maps, target pattern 
% for design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sensTemp = reshape(sens,[Ns Nc]);
sensd = sensTemp(logical(mask(:)),:);

dd = d(logical(mask(:)),:);
xxd = xx(logical(mask(:)));
yyd = yy(logical(mask(:)));
fmapd = fmap(logical(mask(:)));
phsshimd = phsshim(logical(mask)); % Zhipeng
%phsshimd = []; % Zhipeng

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% design small-tip pulses
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% OMP pre-definitions
dimsearch = 64; kfovsearch = 1/prbp.trajres;%8/fov;
[kxs,kys] = ndgrid(-kfovsearch/2:kfovsearch/dimsearch:kfovsearch/2-kfovsearch/dimsearch);
kxs = kxs(:);kys = kys(:);
zi = dimsearch*(dimsearch/2) + dimsearch/2 + 1;
kxs = [kxs(1:zi-1); kxs(zi+1:end)]; % remove DC as OMP candidate
kys = [kys(1:zi-1); kys(zi+1:end)]; % remove DC as OMP candidate
ks = [kxs kys];

[rf,kpe,md,A,sterr,lambda,phs,psb,~,~,compWts,nSVT_sta] = ilgreedylocal(dd*delta_tip*pi/180, ...
    sensd,[xxd yyd],ks,prbp.kmaxdistance,[],Nrungs,fmapd,fb, ...
    b1d,b1d,dt,prbp.beta,prbp.betaadjust,...
    filtertype,'independent',phsshimd,[],mask,algp.compmethod,...
    algp.nthreads,prbp.Ncred,prbp.coilMapping,...
    algp.ncgiters);

b1dnew = {};
for ii = 1:Nrungs
  b1dnew{ii} = b1d;
end
b1d = b1dnew;

if large_tip_flag
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % define large-tip target patterns
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ad = cos(delta_tip*pi/180/2*abs(d));
    bd = sin(delta_tip*pi/180/2*abs(d));
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % mask target patterns for design
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    add = ad(logical(mask(:)),:);
    bdd = bd(logical(mask(:)),:);
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % design large-tip pulses
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if algp.use_lt_oc_zc
        
        fb = 0;
        
        rampsamp = 0;
        prbp.rfss = b1d;
        [rf,kpe,ad,bd,~,~,~,~,~,err,compWts] = ...
            lt_oc_zc(add,bdd,sensd,[xxd yyd],kpe,fmapd,fb,rf,prbp,algp,gpos,gfly,rampsamp,nramp);
        
        %[rf,kpe,ad,bd,err] = lt_oc_zc_new(add,bdd,sensd,[xxd yyd],kpe,fmapd,...
        %    b1d,prbp,algp,gpos,gfly,nramp,prbp.beta,rf,mask,[],[]);
  
    else
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Large tip settings
        if Nrungs <= 5
            betath = 10;
        else
            betath = max(4,4+11-Nrungs);
        end
        % beta threshold for large-tip design
        % (smaller->faster, but may be less accurate).
        % I recommend betath = 10 for Nrungs <= 5, betath = 4
        % for Nrungs > 5. Here we make a smooth transition from 10 to 4.
        
        ltgradopt = 1;
        rampsamp = 0;
        fb = 0;
        phsopt = 'independent';
        [rf,kpe,ad,bd,~,~,~,~,~,~,~,err,compWts,nSVT_lta] = ...
            lt_oc(add,bdd,sensd,[xxd yyd],kpe,fmapd,fb,b1d,z_tbw,prbp.d1,prbp.d2,prbp.dt,gpos,gfly,...
            rampsamp,nramp,prbp.beta,betath,rf,phsopt,[],[],[],[],mask,...
            algp.compmethod,algp.nthreads,ltgradopt,prbp.Ncred,prbp.coilMapping);
        
    end
    
    % embed resulting ad,bd into full arrays
    a = zeros(Ns,Nb);
    b = zeros(Ns,Nb);
    for ii = 1:Nb
        a(logical(mask(:)),ii) = ad(:,ii);
        b(logical(mask(:)),ii) = bd(:,ii);
    end
    
    a = reshape(a,[dimxy Nb]);
    b = reshape(b,[dimxy Nb]);
  
end

rf = reshape(rf,[Nrungs Nc]);

if large_tip_flag
    if strcmp(prbp.pulse_func,'ex') % return a*b
        all_images.images = 2*conj(a).*b;
    elseif strcmp(prbp.pulse_func,'ref') % return spin echo profile
        all_images.images = b.^2;
    elseif strcmp(prbp.pulse_func,'sat,inv') % return Mz profile
        all_images.images = abs(a).^2 - abs(b).^2;
    end
    aout = ad;
    bout = bd;
else
    all_images.images = md;
    aout = [];
    bout = [];
    err = Inf;
end

waveforms.rf = rf;
waveforms.k = kpe;
waveforms.compWts = compWts;

nSVT = nSVT_sta + nSVT_lta;
