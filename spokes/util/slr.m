function rf = slr(np,tb,d1b,d2b,bsc,type,phsslp)

if nargin < 4
  d1b = 0.01;
  d2b = 0.01;
end
if nargin < 5
  bsc = 1-d1b; % scale beta for 180 deg
end
if nargin < 6
  type = 'lin';
end
if nargin < 7
  phsslp = -1;
end

if streq(type,'lin')
  % SLR linear phase pulse design with least-squares filter design
  a1 = 5.309e-3;
  a2 = 7.114e-2;
  a3 = -4.761e-1;
  a4 = -2.66e-3;
  a5 = -5.941e-1;
  a6 = -4.278e-1;
  dib = (a1*log10(d1b)^2+a2*log10(d1b)+a3)*log10(d2b)+(a4*log10(d1b)^2+a5*log10(d1b)+a6);
  
  ftwb = dib/tb; % fractional transition width
  fb = [0 (1-ftwb)*(tb/2) (1+ftwb)*(tb/2) np/2]/(np/2); % freq edges,
                                                        % norm'd to
                                                        % 2*nyquist
  ab = [1 1 0 0];
  wtsb = [1 d1b/d2b];
  
  % could remove this signal processing toolbox dependency by
  % building a Fourier matrix and inverting it to get b. that would
  % also make it easy to specify phase roll
  if phsslp == -1 % use firls
    b = bsc*firls(np-1,fb,ab,wtsb);
  else
    % use explicit least-squares inversion to get target phase ramp
    os = 16;
    A = exp(1i*[-np*os/2:np*os/2-1]'*[-np/2:np/2-1]./(np*os)*2*pi);
    ii = [-np*os/2:np*os/2-1]'/np/os*2;
    d = double(abs(ii) < fb(2));
    ds = double(abs(ii) > fb(3));
    w = d + d2b/d1b*ds;
    b = bsc*(pinv(A'*(diag(w)*A))*(A'*(w.*d.*exp(-1i*2*pi*ii/2*(1/2+64-np/2)))));
    b = b.';
  end
  
  rf = real(b2rf(b));
  
elseif streq(type,'min');
  
  % use John's tool
  rf = real(dzrf(np,tb,'st','min',d1b,d2b));
  
else
  error('pulse type not recognized');
end

