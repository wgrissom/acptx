/* #define NOTHREADS */

#ifndef NOTHREADS
#include <pthread.h> /* for threading */
#endif
#include <math.h>
#include "mex.h"

typedef struct {
  int ns,nc,nr,cmplxbrf;
  double *x,*y,*z;
  double *auxar,*auxai,*auxbr,*auxbi;
  double *afr,*afi,*bfr,*bfi;
  double *drfr,*drfi;
  double *rfwr,*rfwi;
  double *sensr,*sensi;
  double *gareax,*gareay,*gareaz;
  double *omdt;
  double *brfr,*brfi;
  int *ntrf;
  int nstot;
  int nd;
} simparams;


void *blochsim_optcont(void *arg)
{
  simparams *p = (simparams *)arg;
  int i,j,k; /* counters */
  double afr,afi,bfr,bfi;
  double arr,ari,brr,bri;
  double sensr,sensi;
  double sensrfr,sensrfi;
  double art,ait,brt,bit;
  double zomr,zomi,zgr,zgi;
  double *brfr,*brfi;
  double *rfwr,*rfwi;
  double gtotal;
  double b1r,b1i,b1m;
  double c,s,sr,si;
  double da1r,da1i,da2r,da2i,db1r,db1i,db2r,db2i;
  double auxar,auxai,auxbr,auxbi;
  double tmpr,tmpi;
  int cmplxbrf;
  int brfc;
  int nstot;
  int nr;
  int nttotal;
  int l;
  int nd;

  nstot = p->nstot;
  nr = p->nr;
  cmplxbrf = p->cmplxbrf;
  rfwr = p->rfwr;
  rfwi = p->rfwi;
  brfr = p->brfr;
  brfi = p->brfi;

  /* get total num of time points in the pulse */
  nttotal = 0;
  for(j = 0;j < nr-1;j++)
    nttotal += p->ntrf[j];

  for(i = 0;i < p->ns;i++){ /* loop over spatial locs */
    
    /* initialize reverse a, b */
    arr = 1;ari = 0;brr = 0;bri = 0;

    afr = p->afr[i];afi = p->afi[i];
    bfr = p->bfr[i];bfi = p->bfi[i];

    zomr = cos(p->omdt[i]/2);
    zomi = sin(p->omdt[i]/2);

    auxar = p->auxar[i];
    auxai = p->auxai[i];
    auxbr = p->auxbr[i];
    auxbi = p->auxbi[i];

    brfc = nttotal;

    for(j = p->nr-1;j > -1;j--){ /* loop backwards over rungs */

      /* calculate gradient rotation */
      gtotal = p->x[i]*p->gareax[j] + p->y[i]*p->gareay[j];
      if(nd == 3) gtotal += p->z[i]*p->gareaz[j];
      zgr = cos(gtotal/2);
      zgi = sin(gtotal/2);

      /* strip off gradient blip from forward sim */
      art = afr*zgr + afi*zgi;
      ait = afi*zgr - afr*zgi;
      afr = art;afi = ait;
      brt = bfr*zgr - bfi*zgi;
      bit = bfi*zgr + bfr*zgi;
      bfr = brt;bfi = bit;	            

      /* add gradient blip to backward sim */
      art = arr*zgr - ari*zgi;
      ait = ari*zgr + arr*zgi;
      arr = art;ari = ait;
      brt = brr*zgr - bri*zgi;
      bit = bri*zgr + brr*zgi;
      brr = brt;bri = bit;	      

      /* get shimmed b1 for this rung */
      sensrfr = 0;sensrfi = 0;
      for(k = 0;k < p->nc;k++){
	    sensr = p->sensr[i+k*nstot];
	    sensi = p->sensi[i+k*nstot];
	    sensrfr += sensr*rfwr[j+k*nr] - sensi*rfwi[j+k*nr];
	    sensrfi += sensi*rfwr[j+k*nr] + sensr*rfwi[j+k*nr];
      }

      for(k = p->ntrf[j]-1;k > -1;k--){ /* loop over subpulse time */

	    /* strip off-resonance from forward sim */
	    art = afr*zomr + afi*zomi;
	    ait = afi*zomr - afr*zomi;
	    afr = art;afi = ait;
	    brt = bfr*zomr - bfi*zomi;
	    bit = bfi*zomr + bfr*zomi;
	    bfr = brt;bfi = bit;	

	    /* add off-resonance to backward sim */
	    art = arr*zomr - ari*zomi;
	    ait = ari*zomr + arr*zomi;
	    arr = art;ari = ait;
	    brt = brr*zomr - bri*zomi;
	    bit = bri*zomr + brr*zomi;
	    brr = brt;bri = bit;	

	    /* weight rf by subpulse */
	    b1r = brfr[brfc+k]*sensrfr; 
	    b1i = brfr[brfc+k]*sensrfi;
	    if(cmplxbrf == 1){
	      b1r -= brfi[brfc+k]*sensrfi;
	      b1i += brfi[brfc+k]*sensrfr;
	    }
	        
	    /* calculate RF rotation params */
	    b1m = sqrt(b1r*b1r + b1i*b1i);
	    c = cos(b1m/2);
	    s = sin(b1m/2);
	    if(b1m > 0){
	      sr = -s*b1i/b1m;
	      si = s*b1r/b1m;
	    }else{
	      sr = 0;si = 0;
	    }

	    /* strip off current rf rotation from forward sim */
	    art = afr*c + bfr*sr + bfi*si;
	    ait = afi*c + bfi*sr - bfr*si;
	    brt = -afr*sr + afi*si + bfr*c;
	    bit = -afr*si - afi*sr + bfi*c;
	    afr = art;afi = ait;
	    bfr = brt;bfi = bit;

	    /* calculate derivatives (incomplete) */ 
	    tmpr = -bfr*ari - bfi*arr;
	    tmpi = -bfr*arr + bfi*ari;
	    da1r = tmpr*auxar - tmpi*auxai;
	    da1i = tmpr*auxai + tmpi*auxar;
	    tmpr = -afr*bri + afi*brr;
	    tmpi = afr*brr + afi*bri;
	    da2r = tmpr*auxar - tmpi*auxai;
	    da2i = tmpr*auxai + tmpi*auxar;
	    tmpr = -bri*bfr - brr*bfi;
	    tmpi = -brr*bfr + bri*bfi;
	    db1r = tmpr*auxbr - tmpi*auxbi;
	    db1i = tmpr*auxbi + tmpi*auxbr;
	    tmpr = afr*ari - afi*arr;
	    tmpi = -afr*arr - afi*ari;
	    db2r = tmpr*auxbr - tmpi*auxbi;
	    db2i = tmpr*auxbi + tmpi*auxbr;
	    /* loop through coils to calculate derivatives */
	    for(l = 0;l < p->nc;l++){
	      sensr = p->sensr[i+l*nstot];
	      sensi = p->sensi[i+l*nstot];
          tmpr = (da2r + db2r + da1r + db1r)*sensr + (da2i + db2i - da1i - db1i)*sensi;
          tmpi = -(da2r + db2r + da1r + db1r)*sensi + (da2i + db2i - da1i - db1i)*sensr;
	      /* tmpr = (da2r + db2r + da1r + db1r)*sensr + (da2i + db2i + da1i + db1i)*sensi; */
	      /* tmpi = -(da2r + db2r + da1r + db1r)*sensi + (da2i + db2i + da1i + db1i)*sensr; */
	      p->drfr[l*p->nr+j] += brfr[brfc+k]*tmpr/2;
	      p->drfi[l*p->nr+j] += brfr[brfc+k]*tmpi/2;
	      if(cmplxbrf == 1){
	        p->drfr[l*p->nr+j] += brfi[brfc+k]*tmpi/2;
	        p->drfi[l*p->nr+j] -= brfi[brfc+k]*tmpr/2;
	      }	  
	    }	

	    /* add current rf rotation to backward sim */
	    art = arr*c - brr*sr - bri*si;
	    ait = ari*c + bri*sr - brr*si;
	    brt = arr*sr + ari*si + brr*c;
	    bit = arr*si - ari*sr + bri*c;
	    arr = art;ari = ait;
	    brr = brt;bri = bit;
	
      } /* loop over subpulse time */
      
      if(j > 0)
	    brfc -= p->ntrf[j-1];

    } /* loop backwards over rungs */

  } /* loop over spatial locs */

#ifndef NOTHREADS
  pthread_exit(NULL);
#endif
}

void mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{

  double *brfr,*brfi,*rfwr,*rfwi,*sensr,*sensi,*garea,*xx,*omdt,*auxar,*auxai,*auxbr,*auxbi,*afr,*afi,*bfr,*bfi;
  int nt,nd,nc,ns,nr,nttot; /* nthreads,ndims,ncoils,nspatiallocs */
  int *nst; /* number of spatial locs in each thread */
  double *ntrf; /* number of time points in each subpulse */
  int i,j; /* counters */
  double *drfr,*drfi; /* pointers to real/imag output drf */
  simparams *p; /* structures passed to sim threads */
#ifndef NOTHREADS
  pthread_t *threads;
#endif
  int rc;
  int cmplxbrf;

  if (nrhs != 12)
    mexErrMsgTxt("12 Input arguments: brf,ntrf,rfw,sens,garea,xx,omdt,nthreads,auxa,auxb,af,bf");
  if (nlhs != 1)
    mexErrMsgTxt("1 Output argument: drf");

  /* process input args */
  brfr = mxGetPr(prhs[0]);
  cmplxbrf = mxIsComplex(prhs[0]);
  if(cmplxbrf == 1)
    brfi = mxGetPi(prhs[0]);
  ntrf = mxGetPr(prhs[1]);
  if(mxIsComplex(prhs[2]) == 0)
    mexErrMsgTxt("rfw must be complex.");
  rfwr = mxGetPr(prhs[2]);
  rfwi = mxGetPi(prhs[2]);
  sensr = mxGetPr(prhs[3]);
  if(mxIsComplex(prhs[3]) == 0)
    mexErrMsgTxt("sens must be complex.");
  sensi = mxGetPi(prhs[3]);
  garea = mxGetPr(prhs[4]);
  xx = mxGetPr(prhs[5]);
  omdt = mxGetPr(prhs[6]);
  if(!mxIsComplex(prhs[8]))
    mexErrMsgTxt("auxa must be complex.");
  auxar = mxGetPr(prhs[8]);
  auxai = mxGetPi(prhs[8]);
  if(!mxIsComplex(prhs[9]))
    mexErrMsgTxt("auxb must be complex.");
  auxbr = mxGetPr(prhs[9]);
  auxbi = mxGetPi(prhs[9]);
  if(!mxIsComplex(prhs[10]))
    mexErrMsgTxt("af must be complex.");
  afr = mxGetPr(prhs[10]);
  afi = mxGetPi(prhs[10]);
  if(!mxIsComplex(prhs[11]))
    mexErrMsgTxt("bf must be complex.");
  bfr = mxGetPr(prhs[11]);
  bfi = mxGetPi(prhs[11]);

  /* scalars */
  if(mxGetM(prhs[1]) > 1 & mxGetN(prhs[1]) > 1)
    mexErrMsgTxt("ntrf must be a vector");
  nr = mxGetM(prhs[1]) > mxGetN(prhs[1]) ? mxGetM(prhs[1]) : mxGetN(prhs[1]); /* number of rungs */
  nc = mxGetN(prhs[2]); /* number of coils */
  ns = mxGetM(prhs[3]); /* number of spatial locs */
  nd = mxGetN(prhs[4]); /* number of spatial dims */
  nt = mxGetScalar(prhs[7]); /* number of threads */
  nttot = mxGetM(prhs[0]); /* total number of time points */

  /* check # of dims */
  if (nd != mxGetN(prhs[5]))
    mexErrMsgTxt("Column dims of garea and xx must be equal.");
  /* check # of spatial locs */
  if ((ns != mxGetM(prhs[5])) || (ns != mxGetM(prhs[6])) ||
      (mxGetM(prhs[5]) != mxGetM(prhs[6])))
    mexErrMsgTxt("Row dims of sens, xx and omdt must be equal.");
  /* check # of coils */
  if (nc != mxGetN(prhs[3]))
    mexErrMsgTxt("Column dims of rfw and sens must be equal.");

  /* determine number of spatial locations per thread */
  nst = mxCalloc(nt,sizeof(int));
  for(i = 0;i < nt;i++) nst[i] = (int)(ns/nt);
  if(ns - ((int)(ns/nt))*nt > 0) /* need to add remainder to last thread */
    nst[nt-1] += ns - ((int)(ns/nt))*nt;
  
  /* allocate space for solutions */
  plhs[0] = mxCreateDoubleMatrix(nc*nr,1,mxCOMPLEX);
  drfr = mxGetPr(plhs[0]);
  drfi = mxGetPi(plhs[0]);

  /* allocate parameter space, copy p into it */
  p = mxCalloc(nt,sizeof(simparams));
  for(i = 0;i < nt;i++){
    
    /* scalars */
    p[i].ns = nst[i];
    p[i].nstot = ns;
    p[i].nc = nc;
    p[i].nr = nr;
    p[i].cmplxbrf = cmplxbrf;
    p[i].nd = nd;
    
    /* pointers to spatial locations */
    p[i].x = &xx[i*((int)(ns/nt))];
    p[i].y = &xx[i*((int)(ns/nt))+ns];
    if(nd == 3)
      p[i].z = &xx[i*((int)(ns/nt))+2*ns];

    /* pointers to sensitivities */
    p[i].sensr = &sensr[i*((int)(ns/nt))];
    p[i].sensi = &sensi[i*((int)(ns/nt))];

    /* pointer to off-resonance */
    p[i].omdt = &omdt[i*((int)(ns/nt))];

    /* pointer to auxa, auxb */
    p[i].auxar = &auxar[i*((int)(ns/nt))];
    p[i].auxai = &auxai[i*((int)(ns/nt))];
    p[i].auxbr = &auxbr[i*((int)(ns/nt))];
    p[i].auxbi = &auxbi[i*((int)(ns/nt))];

    /* pointer to af, bf */
    p[i].afr = &afr[i*((int)(ns/nt))];
    p[i].afi = &afi[i*((int)(ns/nt))];
    p[i].bfr = &bfr[i*((int)(ns/nt))];
    p[i].bfi = &bfi[i*((int)(ns/nt))];

    /* pointers to output variables */
    p[i].drfr = mxCalloc(nc*nr,sizeof(double));
    p[i].drfi = mxCalloc(nc*nr,sizeof(double));
    for(j = 0;j < nc*nr;j++){
      p[i].drfr[j] = 0;
      p[i].drfi[j] = 0;
    }

    /* copy brf */
    p[i].brfr = mxCalloc(nttot,sizeof(double));
    if(cmplxbrf == 1)
      p[i].brfi = mxCalloc(nttot,sizeof(double));
    for(j = 0;j < nttot;j++){
      p[i].brfr[j] = brfr[j];
      if(cmplxbrf == 1)
	p[i].brfi[j] = brfi[j];
    }

    /* copy ntrf */
    p[i].ntrf = mxCalloc(nr,sizeof(int));
    for(j = 0;j < nr;j++){
      p[i].ntrf[j] = ntrf[j];
    }

    /* copy rfw */
    p[i].rfwr = mxCalloc(nc*nr,sizeof(double));
    p[i].rfwi = mxCalloc(nc*nr,sizeof(double));
    for(j = 0;j < nc*nr;j++){ 
      p[i].rfwr[j] = rfwr[j];
      p[i].rfwi[j] = rfwi[j];
    }

    /* copy garea */
    p[i].gareax = mxCalloc(nr,sizeof(double));
    p[i].gareay = mxCalloc(nr,sizeof(double));
    if(nd == 3)
      p[i].gareaz = mxCalloc(nr,sizeof(double));
    for(j = 0;j < nr;j++){
      p[i].gareax[j] = garea[j];
      p[i].gareay[j] = garea[j+nr];
      if(nd == 3)
	p[i].gareaz[j] = garea[j+2*nr];
    }
	
  }

#ifndef NOTHREADS
  threads = mxCalloc(nt,sizeof(pthread_t));
  for(i=0; i < nt ; i++){
    rc = pthread_create(&threads[i], NULL, blochsim_optcont, (void *)&p[i]);
    /*    rc = 0;*/
    if (rc){
      mexErrMsgTxt("problem with return code from pthread_create()");
    } 
  }

  /* wait for all threads to finish */
  for(i=0; i<nt;i++){
    pthread_join(threads[i],NULL); 
  }
#else
  /* process them serially */
  for(i=0; i < nt; i++) blochsim_optcont((void *)&p[i]);
#endif

  /* combine drf's */
  for(j=0; j< nc*nr;j++){
    drfr[j] = 0;
    drfi[j] = 0;
  }
  for(i=0; i< nt;i++){
    for(j=0; j< nc*nr;j++){
      drfr[j] += p[i].drfr[j];
      drfi[j] += p[i].drfi[j];
    }
  }

  /* release memory */
  for(i=0; i<nt;i++){
    /* release drf */
    mxFree(p[i].drfr);
    mxFree(p[i].drfi);
    /* release brf */
    mxFree(p[i].brfr);
    if(cmplxbrf == 1)
      mxFree(p[i].brfi);
    /* release ntrf */
    mxFree(p[i].ntrf);
    /* release rfw */
    mxFree(p[i].rfwr);
    mxFree(p[i].rfwi);
    /* release garea */
    mxFree(p[i].gareax);
    mxFree(p[i].gareay);
    if(nd == 3)
      mxFree(p[i].gareaz);
  }
  mxFree(nst);
  mxFree(p);
#ifndef NOTHREADS
  mxFree(threads);
#endif

  return;
}

