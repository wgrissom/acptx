function dsumgdt = grad_update_lt_mb(A,B,ad,bd,xx,Ag,Bg,Anb,Bnb,betath,nthreads)

% form matrices from Ag, Bg
Agm = [];Bgm = [];
for ii = 1:length(Ag)
  Agm(:,ii) = col(Ag{ii});
  Bgm(:,ii) = col(Bg{ii});
end

if iscell(xx)
    ndim = size(xx{1},2);
    Nsl = length(xx);
else
    ndim = size(xx,2);
    Nsl = 1;
end
Npe = size(Agm,1); % number of phase encodes
Nt = size(A,2); % number of terms
dsumgdt = zeros(Npe,ndim);
  
if ndim == 2
    if iscell(xx)
        for ss = 1:Nsl
            xx{ss} = [xx{ss} zeros(size(xx{ss},1),1)];
        end
    else
        xx = [xx zeros(size(xx,1),1)];
    end
end

% Note: Find alpha and beta for each slice and sum the alpha and beta
% Gradients (v) and Hessian (s) across spatial locaions 
if iscell(xx)
    prevnx = 1;
    for ss = 1:Nsl
        % alpha
        nx_inds = prevnx:size(xx{ss},1)+prevnx-1;
        [vxa(:,ss),Sxa(:,:,ss),vya(:,ss),Sya(:,:,ss),vza(:,ss),Sza(:,:,ss)] = calcvS_lt(A(nx_inds,:),...
            complexify(ad(nx_inds,:)),xx{ss},Agm,Anb,betath,nthreads);
        % beta
        [vxb(:,ss),Sxb(:,:,ss),vyb(:,ss),Syb(:,:,ss),vzb(:,ss),Szb(:,:,ss)] = calcvS_lt(B(nx_inds,:),...
            complexify(bd(nx_inds,:)),xx{ss},Bgm,Bnb,betath,nthreads);
        prevnx = prevnx+size(xx{ss},1);
    end
else
    % alpha
    [vxa,Sxa,vya,Sya,vza,Sza] = calcvS_lt(A,complexify(ad),xx,Agm,Anb,betath,nthreads);
    % beta
    [vxb,Sxb,vyb,Syb,vzb,Szb] = calcvS_lt(B,complexify(bd),xx,Bgm,Bnb,betath,nthreads);
end
vx = sum(vxa+vxb,2);vy = sum(vya+vyb,2);vz = sum(vza+vzb,2);
Sx = sum(Sxa+Sxb,3);Sy = sum(Sya+Syb,3);Sz = sum(Sza+Szb,3);
dsumgdt(:,1) = inv(Sx)*vx;dsumgdt(:,2) = inv(Sy)*vy;
if ndim == 3
  dsumgdt(:,3) = inv(Sz)*vz;
end
