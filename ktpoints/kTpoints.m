% Script to design a 3D Kt Points compressed pulse (8Ch)
% Copyright Zhipeng Cao and Will Grissom, Vanderbilt University, 2015.

addpath util

load 3Db1b0maps

Nc = size(maps.b1,4); % number of physical coils (Ncoils)
% set initial target phase to quad mode phase
bcb1 = 0;
for ii = 1:Nc
  bcb1 = bcb1 + maps.b1(:,:,:,ii)*exp(1i*(ii-1)*2*pi/Nc).*exp(-1i*angle(maps.b1(:,:,:,1)));
end
maps.phsinit = angle(bcb1);

%%        Algorithm and problem parameters
prbp.delta_tip = 10; % flip angle, degrees
prbp.ndims = ndims(maps.mask); % # spatial dimensions 
prbp.kmaxdistance = [Inf Inf Inf]; % maximum kT-point location
prbp.beta = 10^0; % initial RF regularization parameter
prbp.betaadjust = 1; % automatically adjust RF regularization parameter
prbp.dt = 6.4e-6; % dwell time, sec
prbp.Nsubpts = floor(0.1/prbp.dt/1000);     % # of time points in the subpulses (~0.25 ms subpulses)
prbp.nblippts = 20;                         % number of time points between hard pulses to accomodate gradient blips
prbp.dimxyz = size(maps.b0);
prbp.filtertype = 'Lin phase'; % alternately add kT-points on either size of the DC point
prbp.trajres = 2;                           % maximum spatial frequency of OMP search grid (was 2 for most August 2015 results)

prbp.Npulse = 7; % number of kT-points subpulses

algp.nthreads  = 1;               % number of compute threads (for mex only)
algp.computemethod = 'mex';
algp.ncgiters = 3; % was 3 for most August 2015 results
algp.cgtol = 0.9999;

if ~exist('compApproach','var')
    compApproach = 'arrayComp'; % default compression strategy
end
if ~exist('Ncred','var')
    Ncred = 2; % default Namp
end
if ~exist('coilMappingType','var')
    coilMappingType = 'consecutive'; % 'consecutive' or 'interleaved'
end


%% Run the design
switch compApproach
 
 case 'allChannels'
        
        disp 'Running all-channel design'
        
        prbp.Ncred = Inf; % number of compressed channels (Inf = no compression)
        [all_images, wvfrms] = dzktpts(algp,prbp,maps);
        rfw = wvfrms.rf;
        compWts = eye(Nc);
    
    case 'arrayComp' % proposed array-compressed design
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running compressed %d-channel design\n',prbp.Ncred);
        
        % do a compressed design
        [all_images, wvfrms, nSVT] = dzktpts(algp,prbp,maps);
        rfw = wvfrms.rf;
        compWts = wvfrms.compWts;
        
    case 'oneToN' % 1->N splitting with optimized coil weights, a la Floeser et al
        
        prbp.Ncred = 1; % one channel per split
        switch coilMappingType
            case 'consecutive'
                prbp.coilMapping = reshape(1:Nc,[Nc/Ncred Ncred]); % split array into Nc/Nred consecutive elements                
            case 'interleaved'
                prbp.coilMapping = reshape(1:Nc,[Ncred Nc/Ncred]).'; % split array into Nc/Nred interleaved elements
        end
        
        printf('Running Floeser''s %d-channel design\n',Ncred);
        
        % do a compressed design
        %load oneToNphsinit
        %maps.phsinit = phsinit;
        [all_images, wvfrms] = dzktpts(algp,prbp,maps);
        rfw = wvfrms.rf;
        compWts = wvfrms.compWts;

    case 'quadComp' % Quadrature-based every-other-coil combination
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running quadrature-compressed %d-channel design\n',prbp.Ncred);
        
        % build coil compression matrix
        quadWts = exp(1i*(0:Nc-1)*2*pi/Nc);
        compWts = zeros(Nc,prbp.Ncred);
        switch coilMappingType
         case 'interleaved'
          for ii = 1:prbp.Ncred
            compWts(ii:prbp.Ncred:end,ii) = quadWts(ii:prbp.Ncred:end);
          end
         case 'consecutive'
          for ii = 1:prbp.Ncred
            compWts((ii-1)*Nc/prbp.Ncred+1:ii*Nc/prbp.Ncred,ii) = quadWts((ii-1)*Nc/prbp.Ncred+1:ii*Nc/prbp.Ncred);
          end
        end

        b1Comp = permute(maps.b1,[4 1 2 3]);
        b1Comp = b1Comp(:,:).';
        b1Comp = reshape(b1Comp*compWts,[prbp.dimxyz prbp.Ncred]);

        %figure(1); im(b1Comp); colormap jet;
        
        % And then use combined B1 to design pulse, without compression
        maps.b1 = b1Comp;
        prbp.Ncred = Inf; % Inf = no compression within the design script
        [all_images, wvfrms] = dzktpts(algp,prbp,maps);
        % Convert back to full channel pulses for RF power comparison
        rfw = (compWts*wvfrms.rf.').';
        
    case 'modes' % Quadrature-based every-other-coil combination
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running mode-compressed %d-channel design\n',prbp.Ncred);
        
        % build coil compression matrix
        compWts = exp(1i*(0:Nc-1)'*(1:prbp.Ncred)*2*pi/Nc);
        
        % apply coil compression matrix
        b1Comp = permute(maps.b1,[4 1 2 3]);
        b1Comp = b1Comp(:,:).';
        b1Comp = reshape(b1Comp*compWts,[prbp.dimxyz prbp.Ncred]);

        %figure(1); im(b1Comp); colormap jet;
        
        % And then use combined B1 to design pulse, without compression
        maps.b1 = b1Comp;
        prbp.Ncred = Inf; % Inf = no compression within the design script
        [all_images, wvfrms] = dzktpts(algp,prbp,maps);
        % Convert back to full channel pulses for RF power comparison
        rfw = (compWts*wvfrms.rf.').';
        
    case 'b1SVTComp' % B1-map singular value truncation
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running B1 SVT-compressed %d-channel design\n',prbp.Ncred);
        
        for n = 1:Nc
            tmp = maps.b1(:,:,:,n);
            b1tmp(n,:) = tmp(maps.mask);
        end
        [u,s,v] = svd(b1tmp,'econ');
        compWts = u(:,1:prbp.Ncred)';
        
        b1tmp = compWts * b1tmp; % Right
        
        b1Comp = [];
        for n = 1:prbp.Ncred
            b1Comp(:,:,:,n) = embed(b1tmp(n,:).',maps.mask);
        end
        %figure(1); im(b1Comp); colormap jet;
        
        % And then use combined B1 maps to design pulse
        maps.b1 = b1Comp;
        prbp.Ncred = Inf;
        [all_images, wvfrms] = dzktpts(algp,prbp,maps);
        rfw = wvfrms.rf * compWts;
        
end

farmse = sqrt(mean((abs(all_images.images(maps.mask))/pi*180 - prbp.delta_tip).^2));
rfrms = norm(rfw);

fprintf('Flip angle RMSE: %.4f, RMS RF power: %.4f.\n\n',farmse,rfrms);

%% Calculate SAR
load 300e
tmpSAR = 0 .* cond3D;
tmpP   = 0 .* cond3D;
for mm = 1:prbp.Npulse
    tmpE = repmat(0 .* cond3D,[1,1,1,3]);
    for nn = 1:Nc
        tmpE = tmpE + e13D(:,:,:,:,nn) * rfw(mm,nn);
    end
    tmpP   = tmpP   + cond3D .* sum(abs(tmpE).^2,4);
    tmpSAR = tmpSAR + cond3D .* sum(abs(tmpE).^2,4) ./ dens3D;
end
tmpSAR(isnan(tmpSAR)) = 0;

SARave = SARavesphnp(dens3D, tmpSAR, 0.004, 0.004, 0.004, 10);

rftotal = sum(tmpP(:)) / 100000000;
maxsar = max(SARave(:));

fprintf('Total RF power deposition: %.4f, max 10g SAR: %.4f.\n\n', sum(tmpP(:)) / 100000, max(SARave(:)));

%% Check Initial Results
figure;
subplot(1,3,1); imagesc(flipud(squeeze(abs(all_images.images(:,:,round(end/2)))/pi*180).')); axis image off; caxis([0,prbp.delta_tip+2.5]); colormap jet;
subplot(1,3,2); imagesc(flipud(squeeze(abs(all_images.images(round(end/2),:,:))/pi*180).')); axis image off; caxis([0,prbp.delta_tip+2.5]); colormap jet;
subplot(1,3,3); imagesc(flipud(squeeze(abs(all_images.images(:,round(end/2),:))/pi*180).')); axis image off; caxis([0,prbp.delta_tip+2.5]); colormap jet;




