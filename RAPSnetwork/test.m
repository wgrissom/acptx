% script to test the acpTx_RAPS_network function

weights = [0.13 0.55 0.49 1]; % Channel 2 network from Yan, MRM 2016, Fig. 6
%weights = [weights weights];
%weights = weights(2:end); % to test odd #
%weights = rand(1,8); % random between 0 and 1
% compute the network. the network array is (Nc-1) x Nc,
% where rows correspond to stages, and columns to coils.
% weights must all be positive!
%insertionLoss = 0.3; % insertion loss in dB
insertionLoss = 0; % inserion loss in dB
type = 'standard';
%type = 'onePerLevel';
network = acpTx_RAPS_network(weights,type,insertionLoss);

switch type
    
    case 'onePerLevel'
        
        % get the overall weight for each coil
        Ncoils = length(weights);
        Nstages = Ncoils-1;
        weightsConstructed = zeros(1,Ncoils);
        networkIter = network;
        insertionLossLinear = 10^(-insertionLoss/20);
        for ii = 1:Nstages-1
            
            % find the entry that terminates here; put it in that coil's weight
            weightsConstructed(networkIter(ii,:) > 0 & networkIter(ii+1,:) == 0) = ...
                networkIter(ii,networkIter(ii,:) > 0 & networkIter(ii+1,:) == 0);
            
            % find the entry that feeds the next stage, and multiply it downstream
            % into the rest of the network
            nextStageConnection = find(networkIter(ii,:) > 0 & ...
                networkIter(ii+1,:) > 0);
            networkIter(ii+1:end,:) = networkIter(ii+1:end,:)*...
                network(ii,nextStageConnection)*insertionLossLinear;
            
        end
        weightsConstructed(networkIter(end,:) > 0) = networkIter(end,networkIter(end,:) > 0);
        
        % compare to target weights after normalization
        disp 'Test Error (%):'
        100*norm(weights./max(weights) - weightsConstructed./max(weightsConstructed))./norm(weights./max(weights))
        
    case 'standard'
        
        % get the overall weight for each coil
        % loop through stages, multiplying each level by weights above it
        [Nstages,Ncoils] = size(network);
        networkIter = network;
        for ii = 1:Nstages-1
            % duplicate weights into non-zero entries
            for jj = 1:Ncoils/(2^ii):Ncoils
                networkIter(ii,jj+1:jj+Ncoils/(2^ii)-1) = networkIter(ii,jj);
            end
        end
        weightsConstructed = prod(networkIter,1);
        % truncate constructed weights if not power of 2
        weightsConstructed = weightsConstructed(1:length(weights));
        
        % compare to target weights after normalization
        disp 'Test Error (%):'
        100*norm(weights./max(weights) - weightsConstructed./max(weightsConstructed))./norm(weights./max(weights))

end