% Script to do multi-slice RF shimming
% Copyright Zhipeng Cao and Will Grissom, Vanderbilt University, 2015.

load headb1_all

[dimxy(1),dimxy(2),Nsl,Nc,Nsubj] = size(maps.b1); % number of physical coils (Ncoils)

% normalize b1 by median so that regularization params can be 
% meaningfully reported
for ii = 1:Nsubj
  tmp = maps.b1(:,:,:,:,ii);
  maps.b1(:,:,:,:,ii) = maps.b1(:,:,:,:,ii)./median(abs(tmp(repmat(maps.mask(:,:,:,ii),[1 1 1 Nc])))); 
end
% remove phase of first coil
for ii = 1:Nsubj
  for jj = 1:Nc
    maps.b1(:,:,:,jj,ii) = maps.b1(:,:,:,jj,ii).*exp(-1i*angle(maps.b1(:,:,:,1,ii)));
  end
end

%% Algorithm and problem parameters
if ~exist('compApproach','var')
  compApproach = 'arrayComp'; % default compression strategy
end
if ~exist('Ncred','var')
  Ncred = 2;%Nc:-1:1;%1:Nc; % range of Namps to design for
end
if ~exist('leftOutInd','var')
  leftOutInd = 3; % subject to leave out of compression
end

algp.beta = 0;%10^1;
algp.tol = 0.99999;
algp.ncgiters = 3;
  
%% Run the design
switch compApproach
 
 case 'b1SVTComp'

  rfComp = {};
  mComp = {};
  for ii = 1:length(Ncred)
    
    mapst = maps;
    
    prbp.Ncred = Ncred(ii);
    
    % get compressed B1+ maps for this number channels
    for n = 1:Nc
      tmp = squeeze(maps.b1(:,:,:,n,[1:leftOutInd-1 leftOutInd+1:end]));
      b1tmpAll(n,:) = tmp(logical(maps.mask(:,:,:,[1:leftOutInd-1 leftOutInd+1:end])));
      tmp = squeeze(maps.b1(:,:,:,n,leftOutInd));
      b1tmpLeftOut(n,:) = tmp(logical(maps.mask(:,:,:,leftOutInd)));
    end
    [u,s,v] = svd(b1tmpAll,'econ');
    compWts = u(:,1:prbp.Ncred)';
    %compWts = [eye(prbp.Ncred);zeros(Nc - prbp.Ncred,prbp.Ncred)]';
    
    b1tmpLeftOut = compWts * b1tmpLeftOut; % Right
    b1tmpLeftOut = b1tmpLeftOut.*repmat(exp(-1i*angle(b1tmpLeftOut(1,:))),[prbp.Ncred 1]);
    
    b1Comp = [];
    for n = 1:prbp.Ncred
      b1Comp(:,:,:,n) = embed(b1tmpLeftOut(n,:).',maps.mask(:,:,:,leftOutInd));
    end
    %figure(1); im(b1Comp); colormap jet;
    
    % run the shim with these compressed b1 maps
    mapst.b1 = b1Comp;
    %if prbp.Ncred == Ncred(1)
    %  load phsinit_2;
    %  mapst.phsinit = phsinit;
    %else
    %  mapst.phsinit = angle(tmpmComp{1});
    %end
    %load rf32svt
    %maps.phsinit = 0*maps.mask;
    %for jj = 1:Nsl
    %  tmp = 0;
    %  for ii = 1:Nc
    %    tmp = tmp + b1Comp(:,:,jj,ii)*rf32svt(jj,ii);
    %  end
    %  maps.phsinit(:,:,jj) = angle(tmp);
    %end
    %prbp.Ncred = 1:32;
    mapst.b1 = b1Comp;
    mapst.mask = maps.mask(:,:,:,leftOutInd);
    if prbp.Ncred == Nc
      load phsinit_b1svtcomp_1;
      mapst.phsinit = phsinit;
    else
      mapst.phsinit = angle(mCompLeftOut{prbp.Ncred+1});
    end
    [tmprfComp,tmpmComp,errComp(prbp.Ncred),powComp(prbp.Ncred)] = msShim_arrayComp(mapst,prbp,algp);
    rfCompLeftOut{prbp.Ncred} = tmprfComp{end};
    mCompLeftOut{prbp.Ncred} = tmpmComp{end};
    
  end
  
 case 'arrayComp'

  % remove one of the subject's b1 maps, get the compressed array
  mapst = maps;
  mapst.b1 = mapst.b1(:,:,:,:,[1:leftOutInd-1 leftOutInd+1:end]);
  mapst.b1 = permute(mapst.b1,[1 2 4 3 5]); % swap coil and slices dims
  mapst.b1 = mapst.b1(:,:,:,:); % collapse slices and subjects into one dim
  mapst.b1 = permute(mapst.b1,[1 2 4 3]); % move coils back to last dim
  
  mapst.mask = mapst.mask(:,:,:,[1:leftOutInd-1 leftOutInd+1:end]);
  mapst.mask = mapst.mask(:,:,:); % collapse slice and subjects into one dim
  
  prbp.Ncred = 1:Ncred;

  [rfCompAll,mCompAll,errCompAll,powCompAll,nSVTSvAll,compWtsSvAll] = msShim_arrayComp(mapst,prbp,algp);

  % compress the array and shim the left-out subject
  mapst = maps;
  mapst.mask = maps.mask(:,:,:,leftOutInd);
  mapst.b1 = zeros([dimxy Nsl Ncred]);
  for ii = 1:Ncred
    for jj = 1:Nc
      mapst.b1(:,:,:,ii) = mapst.b1(:,:,:,ii) + compWtsSvAll{end}(jj,ii)*maps.b1(:,:,:,jj,leftOutInd);
    end
  end
  prbp.Ncred = 1:Ncred;
  [rfCompLeftOut,mCompLeftOut,errCompLeftOut,powCompLeftOut,nSVTSvLeftOut,compWtsSvLeftOut] = msShim_arrayComp(mapst,prbp,algp);
  
 case 'allChannels'
  
  % run this subject with all channels (do all to get better init phase for 32ch)
  mapst = maps;
  mapst.b1 = mapst.b1(:,:,:,:,leftOutInd);
  mapst.mask = mapst.mask(:,:,:,leftOutInd);
  %load phsinit_multsubj_2;
  %mapst.phsinit = phsinit;
  prbp.Ncred = 1:Nc;
  algp.ncgiters = 3;
  [rfComp,mComp,errComp,powComp,nSVTSv,compWtsSv] = msShim_arrayComp(mapst,prbp,algp);
  
end
