% Script to do multi-slice RF shimming
% Copyright Zhipeng Cao and Will Grissom, Vanderbilt University, 2015.

load headb1

[dimxy(1),dimxy(2),Nsl,Nc] = size(maps.b1); % number of physical coils (Ncoils)

% normalize b1 by median so that regularization params can be 
% meaningfully reported
maps.b1 = maps.b1./median(abs(maps.b1(repmat(maps.mask,[1 1 1 Nc])))); 

%% Algorithm and problem parameters
if ~exist('compApproach','var')
  compApproach = 'arrayComp'; % default compression strategy
end
if ~exist('Ncred','var')
  Ncred = 1:Nc;%Nc:-1:1;%1:Nc; % range of Namps to design for
end

algp.beta = 0;
algp.tol = 0.99999;

%% Run the design
switch compApproach
 
 case 'b1SVTComp'

  rfComp = {};
  mComp = {};
  for ii = 1:length(Ncred)
    
    mapst = maps;
    
    prbp.Ncred = Ncred(ii);
    
    % get compressed B1+ maps for this number channels
    for n = 1:Nc
      tmp = maps.b1(:,:,:,n);
      b1tmp(n,:) = tmp(maps.mask);
    end
    [u,s,v] = svd(b1tmp,'econ');
    compWts = u(:,1:prbp.Ncred)';
    
    b1tmp = compWts * b1tmp; % Right
    b1tmp = b1tmp.*repmat(exp(-1i*angle(b1tmp(1,:))),[prbp.Ncred 1]);
    
    b1Comp = [];
    for n = 1:prbp.Ncred
      b1Comp(:,:,:,n) = embed(b1tmp(n,:).',maps.mask);
    end
    %figure(1); im(b1Comp); colormap jet;
    
    % run the shim with these compressed b1 maps
    mapst.b1 = b1Comp;
    if prbp.Ncred == Ncred(1)
      load phsinit_2;
      mapst.phsinit = phsinit;
    else
      mapst.phsinit = angle(tmpmComp{1});
    end
    %load rf32svt
    %maps.phsinit = 0*maps.mask;
    %for jj = 1:Nsl
    %  tmp = 0;
    %  for ii = 1:Nc
    %    tmp = tmp + b1Comp(:,:,jj,ii)*rf32svt(jj,ii);
    %  end
    %  maps.phsinit(:,:,jj) = angle(tmp);
    %end
    [tmprfComp,tmpmComp,errComp(prbp.Ncred),powComp(prbp.Ncred)] = msShim_arrayComp(mapst,prbp,algp);
    rfComp{prbp.Ncred} = tmprfComp{1};
    mComp{prbp.Ncred} = tmpmComp{1};
    
  end
  
 case 'arrayComp'

  prbp.Ncred = Ncred;
  algp.ncgiters = 3;
  [rfComp,mComp,errComp,powComp,nSVTSv] = msShim_arrayComp(maps,prbp,algp);

end
