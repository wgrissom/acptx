addpath util/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mls = 0;            % switch to use magnitude least-squares
%roughbeta = 10^1.75;   % RF waveform roughness penalty, measured spiral
%roughbeta = 10^2.75; % xfov = 6 generated spiral
%roughbeta = 10^2.75; % xfov = 8 generated spiral
roughbeta = 10^2.25; % nyu, xfov = 8

flipAngle = 30;

genfigs = false;
if ~exist('compApproach','var')
  compApproach = 'arrayComp';
  genfigs = true;
end
if ~exist('Ncred','var')
  Ncred = 2;          % # compressed channels
end
if ~exist('coilMappingType','var')
  coilMappingType = 'consecutive'; % 'consecutive' or 'interleaved'
end
if ~exist('b1MapSelect','var')
  b1MapSelect = '8ch,sim';
  %b1MapSelect = '8ch,nyu';
  %b1MapSelect = '8ch,nyu,sag';
end
if ~exist('kTrajSelect','var')
  %kTrajSelect = '5cm,measured';
  kTrajSelect = 'spiralIn';
  %kTrajSelect = 'hard';
end
if ~exist('dPatternSelect','var')
  dPatternSelect = 'Vandy';
  %dPatternSelect = 'unif';
  %dPatternSelect = 'nyu';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load b1 maps, k-space trajectory, target pattern
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch(b1MapSelect)
  
 case '16ch,exp'
  
  load b1maps;
  fov = 20; % cm, fov of *unaliased* excitation
  
 case '8ch,sim'
  
  load ../ktpoints/3Db1b0maps;
  b1 = squeeze(maps.b1(:,:,floor(size(maps.b1,3)/2),:));
  mask = squeeze(maps.mask(:,:,floor(size(maps.b1,3)/2),:));
  b1 = permute(b1,[2 1 3]);b1 = flip(b1,1);
  mask = permute(mask,[2 1 3]);mask = flip(mask,1);
  fov = maps.fov(1); % cm, fov of *unaliased* excitation
  b0 = 0*mask;
  
 case '8ch,nyu'
  
  load ~/data/nyu/082115/Cal_Data/maps.mat; % phantom
  %load ~/data/nyu/082115/cal_invivo/maps.mat; % in vivo
  Voltage = 45; % phantom
  %Voltage = 75; % in vivo
  hardPulseDur = 0.001;
  b1 = maps.B1/(2*pi*42.58*Voltage*hardPulseDur);
  b1 = b1.*exp(1i*maps.PH);
  b0 = maps.B0./(2*pi);
  fov = 256/10; % cm, fov of unaliased excitation
  mask = logical(maps.mask);
 
 case '8ch,nyu,sag'
  
  load ~/data/nyu/082115/Cal_Sag/maps.mat;
  Voltage = 45;
  hardPulseDur = 0.001;
  b1 = maps.B1/(2*pi*42.58*Voltage*hardPulseDur);
  b1 = b1.*exp(1i*maps.PH);
  b0 = maps.B0./(2*pi);
  fov = 256/10; % cm, fov of unaliased excitation
  mask = logical(maps.mask);
  
end

[dimb1(1),dimb1(2),Nc] = size(b1);

% normalize b1 by median so that regularization params can be 
% meaningfully reported
b1Scale = 1/median(abs(b1(repmat(mask,[1 1 Nc])))); 
b1 = b1*b1Scale;
% apply mask to b1 maps, if not already done
b1 = b1.*repmat(mask,[1 1 Nc]);

switch(kTrajSelect)
 case '5cm,measured'
  % Load measured kspace trajectory and system dwell time
  % k-traj fov is 5 cm (3.4x acceleration relative to 17 cm diameter phantom)
  load k_meas
  Nt = size(k,1);
 case 'spiralIn'
  if strcmp(b1MapSelect,'8ch,nyu') || strcmp(b1MapSelect,'8ch,nyu,sag')
    dt = 10e-6;
  else
    dt = 4e-6;
  end
  deltax = fov/50; % spatial resolution of trajectory
  FOVsubsamp = 7; % cm, xfov of spiral
  forwardspiral = 0;
  dorevspiralramp = 0;
  gmax = 4; % g/cm,
  dgdtmax = 8000; % 
  traj = 'spiral';
  get_traj;
  k = k(NN(2)+1:end,:);
  if strcmp(b1MapSelect,'8ch,nyu') || strcmp(b1MapSelect,'8ch,nyu,sag')
    k = k(:,1)+1i*k(:,2);
    k = k.*exp(1i*-1/180*pi);
    k = [real(k) imag(k)];
  end
  Nt = size(k,1);
  %if strcmp(b1MapSelect,'8ch,nyu')
  %  dt = 10e-6;
  %  g = g(1:2:end,:);
  %  k = k(2:2:end,:);
  %  NN = ceil(NN/2);
  %  Nt = Nt/2;
  %end
  tb0 = (0:size(k,1)-1)*dt;
  Lseg = 4;
 case 'hard'
  Nt = 100;
  dt = 10e-6;
  k = zeros(Nt,2);
  g = zeros(Nt,2);
  NN = [Nt 0];
  b0 = 0*b0;
end

switch dPatternSelect
  case 'Vandy'
   % load target pattern
   load despattern % Vandy logo
   dim = size(d);
   if strcmp(b1MapSelect,'8ch,nyu') || strcmp(b1MapSelect,'8ch,nyu,sag')
     d = d.';
   end
 case 'unif'
  dim = [128 128];
  d = ones(dim);
 case 'nyu'
  load nyu_torch;
  dim = size(d);
   if strcmp(b1MapSelect,'8ch,nyu') || strcmp(b1MapSelect,'8ch,nyu,sag')
     d = d.';
   end
   %d = circshift(d,[0 0]);
   %d = fliplr(d);
   %d = circshift(d,[5 0]);
   d = 1-d;
end

% interpolate the B1 maps to the same grid as the target pattern
[xi,yi] = ndgrid(-dim(1)/2:dim(1)/2-1,-dim(2)/2:dim(2)/2-1);
[xb1,yb1] = ndgrid((-dimb1(1)/2:dimb1(1)/2-1)/dimb1(1),(-dimb1(2)/2:dimb1(2)/2-1)/dimb1(2));
b1Int = zeros([dim Nc]);
for ii = 1:Nc
  b1Int(:,:,ii) = interp2(yb1,xb1,b1(:,:,ii),yi/dim(2),xi/dim(1),'linear',0);
end
b1 = b1Int;
b0Int = interp2(yb1,xb1,b0,yi/dim(2),xi/dim(1),'linear',0);
b0 = b0Int;
maskInt = interp2(yb1,xb1,double(mask),yi/dim(2),xi/dim(1),'linear',0);
mask = maskInt > 0.9; % get new mask for higher res b1

% remove the first channels phase from the rest
b1 = b1.*repmat(exp(-1i*angle(b1(:,:,1))),[1 1 Nc]);

% find the middle point of the mask
xmi = round(mean(xi(logical(mask(:)))));
ymi = round(mean(yi(logical(mask(:)))));

% center the target pattern in the mask
d = circshift(d,[xmi ymi]);

% smooth it a bit
d = ift2((hamming(dim(1))*hamming(dim(2))').^2.*ft2(d));

if strcmp(dPatternSelect,'unif')
  d = d.*exp(1i*angle(sum(b1,3)));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up system matrix and penalty matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Build RF waveform roughness penalty matrix for full
% and compressed arrays
R = sqrt(roughbeta)*spdiags([-ones(Nt,1) ones(Nt,1)],[0 1],Nt,Nt) + ...
    sqrt(roughbeta)*speye(Nt);
Rfull = kron(speye(Nc),R);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start the design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nSVT = 0; % number of SVT's
switch compApproach

 case 'oneToN' % 1->N splitting with optimized coil weights, a la Floeser et al

  printf('Running Floeser''s %d-channel design\n',Ncred);
  
  switch coilMappingType
   case 'consecutive'
    coilMapping = reshape(1:Nc,[Nc/Ncred Ncred]); % split array into Nc/Nred consecutive elements
   case 'interleaved'
    coilMapping = reshape(1:Nc,[Ncred Nc/Ncred]).'; % split array into Nc/Nred interleaved elements
  end

  % columnize and mask the b1 maps
  b1 = permute(b1,[3 1 2]);b1 = b1(:,:).';
  b1 = b1(mask,:);

  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1))';
  end
  
  ncgiters = 3;       % # CG Iterations per RF update
  flag = 1;itr = 1;
  rf = zeros(length(k)*Nc,1); costOld = Inf;
  if genfigs
    h = figure(); % get figure handle to display progress
  end
  while flag
    
    % Update RF waveforms
    rf = qpwls_pcg(rf(:),A,1,d(mask),0,Rfull,1,ncgiters,mask); % CG
    rf = reshape(rf(:,end),[length(k) Nc]); % qpwls_pcg returns all iterates
    
    % truncate the svd of the RF matrix
    rft = rf;
    compWts = [];
    for ii = 1:size(coilMapping,2)
      [u,s,v] = svd(rf(:,coilMapping(:,ii)),'econ');
      s(2:end,2:end) = 0; % Hard thresholding
      rft(:,coilMapping(:,ii)) = u*(s*v');
      compWts(:,ii) = conj(v(:,1));
    end
    rf = rft(:);
    tmp = zeros(Nc,Ncred);
    for ii = 1:size(coilMapping,2)
      tmp(coilMapping(:,ii),ii) = compWts(:,ii);
    end
    compWts = tmp;
    
    % calculate excitation pattern
    m = zeros(dim);m(mask) = A*rf(:);
    
    % update target phase if magnitude least-squares
    if mls 
        d = abs(d).*exp(1i*angle(m));
    end
    
    if genfigs
      figure(h);
      tmp = diag(s);
      subplot(131); 
      plot(abs(tmp(1:Ncred+1)));axis square
      xlabel 'Index',ylabel 'Amplitude',title 'Singular Values'
      subplot(132);
      imagesc(abs(m)); axis image; colorbar; caxis([0,1.05*max(abs(d(:)))]);
      title 'Excited pattern'
      subplot(133)
      imagesc(abs(m-d));axis image;colorbar;title 'Error'
      drawnow
    end
    
    % calculate cost
    res = m - d; cost = 1/2*(norm(res(mask))^2 + norm(Rfull*rf(:))^2);
    
    fprintf('%d Channels, iteration %d. Cost: %f. \n',Ncred,itr,cost);
    
    % check if we should stop
    if cost > 0.9999*costOld
        flag = 0;
    else
        costOld = cost;
    end
    
    itr = itr + 1;
    
  end

  finalerr = norm(res)/norm(d);
  finalroughness = norm(Rfull*rf(:));
  fprintf('Final NRMSE: %f%%. Final roughness: %f.\n',finalerr*100,finalroughness);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Redesign the final pulses using the compressed array
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % get the compressed b1 maps
  % REDFLAG Need to embed CompWts into matrix according to coilMapping!!
  b1Comp = b1*compWts;
  
  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp))';
  end
  
  % do the RF design
  ncgiters = floor(length(k)/8); % # of iterations
  Rcomp = kron(compWts,R);
  rf = qpwls_pcg(zeros(length(k)*Ncred,1),A,1,d(mask),0,Rcomp,1,ncgiters); % CG
  rf = reshape(rf(:,end),[length(k) Ncred]); % qpwls_pcg returns all iterates
  
  % calculate excitation pattern
  m = zeros(dim);m(mask) = A*rf(:);
  
  err = norm(m-d)/norm(d);
  roughness = norm(Rcomp*rf(:));
  rf = reshape(rf,[length(k) Ncred])*compWts.';
  pow = sqrt(mean(abs(rf(:)).^2));
  fprintf('oneToN-compressed NRMSE: %0.2f%%. Final roughness: %f.\n',err*100,roughness);
  
 case 'modes'  % Quadrature-based every-other-coil combination
  
  printf('Running mode-compressed %d-channel design\n',Ncred);
  
  % build coil compression matrix
  compWts = exp(1i*(0:Nc-1)'*(1:Ncred)*2*pi/Nc);
  
  % apply coil compression matrix
  b1Comp = permute(b1,[3 1 2]);
  b1Comp = b1Comp(:,:).';
  b1Comp = reshape(b1Comp*compWts,[dim Ncred]);

  % columnize and mask the b1 maps
  b1Comp = permute(b1Comp,[3 1 2]);b1Comp = b1Comp(:,:).';
  b1Comp = b1Comp(mask,:);

  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp))';
  end
      
  ncgiters = floor(length(k)/8); % # of iterations
  Rcomp = kron(compWts,R);
  rf = qpwls_pcg(zeros(length(k)*Ncred,1),A,1,d(mask),0,Rcomp,1,ncgiters); % CG
  rf = reshape(rf(:,end),[length(k) Ncred]); % qpwls_pcg returns all iterates
  
  % calculate excitation pattern
  m = zeros(dim);m(mask) = A*rf(:);

  % get original pulses back
  rf = rf*compWts.';
  
  err = norm(m-d)/norm(d);
  roughness = norm(Rfull*rf(:));
  pow = sqrt(mean(abs(rf(:)).^2));
  fprintf('Modes NRMSE: %0.2f%%. All-channels roughness: %f.\n',err*100,roughness);  
  
 case 'quadComp'
  
  printf('Running quadrature-compressed %d-channel design\n',Ncred);
  
  % build coil compression matrix
  quadWts = exp(1i*(0:Nc-1)*2*pi/Nc);
  compWts = zeros(Nc,Ncred);
  switch coilMappingType
   case 'interleaved'
    for ii = 1:Ncred
      compWts(ii:Ncred:end,ii) = quadWts(ii:Ncred:end);
    end
   case 'consecutive'
    for ii = 1:Ncred
      compWts((ii-1)*Nc/Ncred+1:ii*Nc/Ncred,ii) = quadWts((ii-1)*Nc/Ncred+1:ii*Nc/Ncred);
    end
  end
  b1Comp = permute(b1,[3 1 2]);
  b1Comp = b1Comp(:,:).';
  b1Comp = reshape(b1Comp * compWts,[dim Ncred]);
  %figure(1); im(b1Comp); colormap jet;

  % columnize and mask the b1 maps
  b1Comp = permute(b1Comp,[3 1 2]);b1Comp = b1Comp(:,:).';
  b1Comp = b1Comp(mask,:);

  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp))';
  end
      
  ncgiters = floor(length(k)/8); % # of iterations
  Rcomp = kron(compWts,R);
  rf = qpwls_pcg(zeros(length(k)*Ncred,1),A,1,d(mask),0,Rcomp,1,ncgiters); % CG
  rf = reshape(rf(:,end),[length(k) Ncred]); % qpwls_pcg returns all iterates
  
  % calculate excitation pattern
  m = zeros(dim);m(mask) = A*rf(:);
  
  % get original pulses back
  rf = rf*compWts.';
  
  err = norm(m-d)/norm(d);
  roughness = norm(Rfull*rf(:));
  pow = sqrt(mean(abs(rf(:)).^2));
  fprintf('Quadrature NRMSE: %0.2f%%. All-channels roughness: %f.\n',err*100,roughness);  
  
 case 'b1SVTComp'
  
  printf('Running B1 SVT-compressed %d-channel design\n',Ncred);

  b1tmp = [];
  for n = 1:Nc
    tmp = b1(:,:,n);
    b1tmp(n,:) = tmp(mask);
  end
  [u,s,v] = svd(b1tmp,'econ');
  compWts = conj(u(:,1:Ncred));
  
  b1tmp = b1tmp.' * compWts; % Right
  
  b1Comp = [];
  for n = 1:Ncred
    b1Comp(:,:,n) = embed(b1tmp(:,n),mask);
  end
  %figure(1); im(b1Comp); colormap jet;

  % columnize and mask the b1 maps
  b1Comp = permute(b1Comp,[3 1 2]);b1Comp = b1Comp(:,:).';
  b1Comp = b1Comp(mask,:);

  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp))';
  end
      
  ncgiters = floor(length(k)/8); % # of iterations
  Rcomp = kron(compWts,R);
  rf = qpwls_pcg(zeros(length(k)*Ncred,1),A,1,d(mask),0,Rcomp,1,ncgiters); % CG
  rf = reshape(rf(:,end),[length(k) Ncred]); % qpwls_pcg returns all iterates
  
  % calculate excitation pattern
  m = zeros(dim);m(mask) = A*rf(:);
  
  % get original pulses back
  rf = rf*compWts.';
  
  err = norm(m-d)/norm(d);
  roughness = norm(Rfull*rf(:));
  pow = sqrt(mean(abs(rf(:)).^2));
  fprintf('B1 SVT NRMSE: %0.2f%%. All-channels roughness: %f.\n',err*100,roughness);  
  
 case 'allChannels' % Design with all original Tx channels

  disp 'Running all-channels design'

  compWts = eye(Nc);
  
  % columnize and mask the b1 maps
  b1 = permute(b1,[3 1 2]);b1 = b1(:,:).';
  b1 = b1(mask,:);
  
  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1))';
  end
  
  ncgiters = floor(length(k)/8);       % # CG Iterations per RF update
  rf = qpwls_pcg(zeros(length(k)*Nc,1),A,1,d(mask),0,Rfull,1,ncgiters,mask); % CG
  rf = reshape(rf(:,end),[length(k) Nc]); % qpwls_pcg returns all iterates
  
  % calculate excitation pattern
  m = zeros(dim);m(mask) = A*rf(:);
  
  %A = Gmri_SENSE([-k(:,1) k(:,2)],mask,'fov',fov,'sens',conj(b1))';
  %m2 = zeros(dim);m2(mask) = A*rf(:);
  
  err = norm(m-d)/norm(d);
  roughness = norm(Rfull*rf(:));
  pow = sqrt(mean(abs(rf(:)).^2));
  fprintf('All-channels NRMSE: %0.2f%%. All-channels roughness: %f.\n',err*100,roughness);
  
 case 'arrayComp' % array-compressed (proposed approach)
  
  printf('Running array-compressed %d-channel design\n',Ncred);

  % columnize and mask the b1 maps
  b1 = permute(b1,[3 1 2]);b1 = b1(:,:).';
  b1 = b1(mask,:);

  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1))';
  end
  
  ncgiters = 3;       % # CG Iterations per RF update
  flag = 1;itr = 1;
  rf = zeros(length(k)*Nc,1); costOld = Inf;
  if genfigs
    h = figure(); % get figure handle to display progress
  end
  while flag
    
    % Update RF waveforms
    rf = qpwls_pcg(rf(:),A,1,d(mask),0,Rfull,1,ncgiters,mask); % CG
    rf = rf(:,end); % qpwls_pcg returns all iterates
    
    % truncate the svd of the RF matrix
    rf = reshape(rf,[length(k) Nc]);
    [u,s,v] = svd(rf,'econ');
    s(Ncred+1:end,Ncred+1:end) = 0;
    rf = u*(s*v');
    compWts = conj(v(:,1:Ncred));
    nSVT = nSVT + 1;
    
    % calculate excitation pattern
    m = zeros(dim);m(mask) = A*rf(:);
    
    % update target phase if magnitude least-squares
    if mls 
        d = abs(d).*exp(1i*angle(m));
    end
    
    if genfigs
      figure(h);
      tmp = diag(s);
      subplot(131); 
      plot(abs(tmp(1:Ncred+1)));axis square
      xlabel 'Index',ylabel 'Amplitude',title 'Singular Values'
      subplot(132);
      imagesc(abs(m)); axis image; colorbar; caxis([0,1.05*max(abs(d(:)))]);
      title 'Excited pattern'
      subplot(133)
      imagesc(abs(m-d));axis image;colorbar;title 'Error'
      drawnow
    end
    
    % calculate cost
    res = m - d; cost = 1/2*(norm(res(mask))^2 + norm(Rfull*rf(:))^2);
    
    fprintf('%d Channels, iteration %d. Cost: %f. \n',Ncred,itr,cost);
    
    % check if we should stop
    if cost > 0.9999*costOld
        flag = 0;
    else
        costOld = cost;
    end
    
    itr = itr + 1;
    
  end

  finalerr = norm(res)/norm(d);
  finalroughness = norm(Rfull*rf(:));
  fprintf('Final NRMSE: %f%%. Final roughness: %f.\n',finalerr*100,finalroughness);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Redesign the final pulses using the compressed array
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % get the compressed b1 maps
  b1Comp = b1*compWts;

  % Build system matrix
  if max(abs(b0(:)) > 0)
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp),'ti',tb0,'zmap',-1i*2*pi*b0,'L',Lseg)';
  else
    A = Gmri_SENSE(k,mask,'fov',fov,'sens',conj(b1Comp))';
  end
  
  % do the RF design
  ncgiters = floor(length(k)/8); % # of iterations
  Rcomp = kron(compWts,R);
  rf = qpwls_pcg(zeros(length(k)*Ncred,1),A,1,d(mask),0,Rcomp,1,ncgiters); % CG
  rf = reshape(rf(:,end),[length(k) Ncred]); % qpwls_pcg returns all iterates
  
  % calculate excitation pattern
  m = zeros(dim);m(mask) = A*rf(:);
  
  err = norm(m-d)/norm(d);
  roughness = norm(Rcomp*rf(:));
  rf = rf * compWts.';
  pow = sqrt(mean(abs(rf(:)).^2));
  fprintf('Array-compressed NRMSE: %0.2f%%. Final roughness: %f.\n',err*100,roughness);
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display final results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if genfigs
  maxamp = max(abs([d(:);m(:)]));
  figure
  subplot(221)
  imagesc(abs(d),[0 maxamp]);axis image;colorbar
  title 'Desired pattern'
  subplot(222)
  imagesc(abs(m),[0 maxamp]);axis image;colorbar
  title(sprintf('Final pattern',Nc));
  subplot(224)
  imagesc(abs(m-d));axis image;colorbar
  title(sprintf('Error\nNRMSE = %0.2f%%',err*100));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate SAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(b1MapSelect,'8ch,sim')
  
  load ../ktpoints/300e
  tmpP   = 0 .* cond3D;
  parfor ii = 1:length(k)
    if rem(ii,50) == 0; fprintf('Calculating SAR for time point %d of %d\n',ii,length(k)); end
    tmpE = 0;
    for nn = 1:Nc
      tmpE = tmpE + e13D(:,:,:,:,nn) * rf(ii,nn);
    end
    tmpP = tmpP + sum(abs(tmpE).^2,4);
  end
  tmpP = tmpP.*cond3D;
  tmpSAR = tmpP./dens3D;
  tmpSAR(isnan(tmpSAR)) = 0;
  
  SARave = SARavesphnp(dens3D, tmpSAR, 0.004, 0.004, 0.004, 10);
  
  rftotal = sum(tmpP(:)) / 100000000;
  maxsar = max(SARave(:));
  
  fprintf('Total RF power deposition: %.4f, max 10g SAR: %.4f.\n\n', rftotal, maxsar);

end

if strcmp(dPatternSelect,'unif')
  rf(1,:) = 0;
end

if strcmp(b1MapSelect,'8ch,nyu') || strcmp(b1MapSelect,'8ch,nyu,sag')
  %% rescale and write out pulses
  c = (flipAngle*pi/180) / (10e-6 * 2*pi*42.58) * b1Scale;
  rf = rf*c;
  rf = [zeros(NN(2),Nc);rf];
  g = [fliplr(g) zeros(sum(NN),1)]*10;
  g = [zeros(2,3);g;zeros(2,3)];
  if strcmp(b1MapSelect,'8ch,nyu,sag')
    g = [0*g(:,1) g(:,2) g(:,1)];
  end
  rf = [zeros(2,Nc);rf;zeros(2,Nc)];
  writepulses_nyu(conj(rf),g,['grissom_ptx_exc.ini,' compApproach ',' coilMappingType],flipAngle,'pTx','G pTx pulse',0,0.5);
end