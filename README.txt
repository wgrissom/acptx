MATLAB Codes to implement array-compressed pulse designs. For more info, or to report any errors or missing files, please contact Will Grissom: will.grissom@vanderbilt.edu. 

The 300e.mat file used to calculate SAR can be downloaded from: https://www.dropbox.com/s/esjholfhp28nsjp/300e.mat?dl=0